﻿using UnityEngine;
using static UnityEngine.InputSystem.InputAction;

namespace Game.Input
{
	public class GameInputController : MonoBehaviour
	{
		[Header("Input Receivers")]
		[SerializeField] private GameInputReceiver[] _inputReceivers = null;

		public void OnPlayerSteerInput(CallbackContext ctx)
		{
			var steerDirection = ctx.ReadValue<float>();

			int length = _inputReceivers.Length;
			for (int i = 0; i < length; i++)
			{
				_inputReceivers[i].SteerDirection = steerDirection;
			}
		}

		public void OnPlayerThrustInput(CallbackContext ctx)
		{
			var thrustInput = ctx.performed && !ctx.canceled;

			int length = _inputReceivers.Length;
			for (int i = 0; i < length; i++)
			{
				_inputReceivers[i].ThrustInput = thrustInput;
			}
		}
	}
}