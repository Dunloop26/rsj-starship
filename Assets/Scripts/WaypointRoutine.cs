﻿using UnityEngine;
using DG.Tweening;
using System;
using System.Collections;
using Dunas.Coroutine;
using static Dunas.Coroutine.CoroutineRunner;

[System.Serializable]
public class WaypointRoutine : Routine
{
  public WaypointRoutine(Transform target, Vector3[] waypoints, float inBetweenDuration, float duration = 1f)
  {
    Target = target;
    Waypoints = waypoints;
    InBetweenDuration = inBetweenDuration;
    Duration = duration;
  }
  public Transform Target { get; private set; }
  public Vector3[] Waypoints { get; }
  public float InBetweenDuration { get; private set; }
  public override float Duration { get; set; }
  public bool UseRelativePositions { get; set; }
  public bool ResetOnEnd { get; set; }
  public Vector3 CurrentTarget => _currentTarget;

  private Vector3 _startPosition;
  private Vector3 _currentTarget;
  private int _currentIndex = 0;
  private Sequence _sequence;

  private bool _stopSequence = false;
  private bool _pauseSequence = false;

  private RunnerObject _runner;

  public WaypointRoutine SetResetOnEnd(bool value)
  {
    ResetOnEnd = value;
    return this;
  }

  public WaypointRoutine SetDuration(float duration)
  {
    Duration = duration;
    return this;
  }

  public WaypointRoutine SetUseRelativePositions(bool relative)
  {
    UseRelativePositions = relative;
    return this;
  }
  public override void StartRoutine()
  {
    _startPosition = Target.position;
    // var position = UseRelativePositions
    // ? _startPosition + Waypoints[Waypoints.Length - 1]
    // : Waypoints[Waypoints.Length - 1];

    // _pauseSequence = false;
    // _stopSequence = false;

    // var startTween = Target.DOMove(position, InBetweenDuration > 0 ? InBetweenDuration : 1f);
    // startTween.OnComplete(() =>
    // {
    //   _sequence = BuildSequence(Waypoints);
    //   _sequence.Play();
    // });

    _runner = CoroutineRunner.BuildRunnerFromInstance(Run());
    _runner.Run();
  }

  private IEnumerator Run()
  {
    WaitWhile pause = new WaitWhile(() => this._pauseSequence);

    int index = 0;

    Vector3 current = Target.position;
    Vector3 target;

    if (UseRelativePositions)
      target = _startPosition + Waypoints[0];
    else
      target = Waypoints[0];

    float elapsed = 0f;
    float percent = 0f;

    Debug.Log($"Warping");

    while (!_stopSequence)
    {
      yield return pause;

      elapsed += Time.deltaTime;
      percent = elapsed / InBetweenDuration;
      percent = Mathf.Clamp01(percent);

      Target.position = Vector3.Lerp(current, target, percent);


      yield return null;

      if (percent >= 1f)
      {
        // TODO: Implementar Looping
        if (index >= Waypoints.Length) break;
        index = (index + 1) % Waypoints.Length;

        elapsed = 0f;
        current = target;

        if (UseRelativePositions)
          target = _startPosition + Waypoints[index];
        else
          target = Waypoints[index];
      }
    }
  }

  private IEnumerator GoToStart()
  {
    float elapsed = 0f;

    float percent = 0f;
    Vector3 current = Target.position;
    while (percent < 1f)
    {
      yield return null;
      elapsed += Time.deltaTime;
      percent = elapsed / InBetweenDuration;
      Target.position = Vector3.Lerp(current, _startPosition, percent);
    }

    percent = 1f;
    Target.position = _startPosition;
  }

  private Sequence BuildSequence(Vector3[] waypoints)
  {
    Sequence seq = DOTween.Sequence();

    int length = waypoints.Length;
    for (int i = 0; i < length; i++)
    {
      Vector3 position = UseRelativePositions
      ? _startPosition + waypoints[i]
      : waypoints[i];

      seq.Append(Target.DOMove(position,
      InBetweenDuration != 0
      ? InBetweenDuration
      : 1f));
    }

    seq.SetLoops(-1).SetAutoKill(false);
    return seq;
  }

  public override void EndRoutine()
  {
    _pauseSequence = false;
    _stopSequence = false;

    _runner.Stop();
    CoroutineRunner.BuildRunnerFromInstance(GoToStart()).Run();

    // _sequence.Pause();
    // _sequence.Kill();
    // Target.DOMove(_startPosition, 1f);
  }
}