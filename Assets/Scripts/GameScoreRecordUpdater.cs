﻿using Dunas.Events;
using TMPro;
using UnityEngine;

namespace Game
{

	public class GameScoreRecordUpdater : MonoBehaviour
	{
		[Header("Dependencies")]
		[SerializeField] private GameScoreCounter _counter = null;
		[Header("Event")]
		[SerializeField] private FloatEvent _scoreUpdate = null;

		public void EvaluateHighScore()
		{
			if (IsNewRecord(_counter.Score))
			{
				_scoreUpdate?.Invoke(_counter.Score);
			}
		}

		public bool IsNewRecord(float score)
		{
			return GamePrefs.Record < score;
		}
	}
}