﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class GamePrefs
{
  public static readonly string RECORD_KEY = "record";

  public static float Record
  {
    get
    {
      return PlayerPrefs.GetFloat(RECORD_KEY, 0f);
    }
    set
    {
      PlayerPrefs.SetFloat(RECORD_KEY, value);
    }
  }
}
