﻿using System;
using System.Collections;
using UnityEngine;

namespace Game
{
	public class BulletTimeChargeController : MonoBehaviour
	{
		[Header("Dependencies")]
		[SerializeField] private BulletTimeAbility _ability = null;
		[SerializeField] private FillController _fillController = null;

		private bool _isAbilityActive = false;

		private void Start()
		{
			if (_fillController == null)
			{
				Debug.LogWarning($"The fill controller has not been defined properly, can't continue", this);
				return;
			}

			BindEvents();
		}

		private void OnDestroy()
		{
			UnbindEvents();
		}

		private void BindEvents()
		{
			if (_ability == null) return;

			_ability.ChargeChange += OnAbilityChargeChange;
			_ability.Activated += OnAbilityActivated;
		}
		private void UnbindEvents()
		{
			if (_ability == null) return;

			_ability.ChargeChange -= OnAbilityChargeChange;
			_ability.Activated -= OnAbilityActivated;
		}

		private void OnAbilityActivated(BulletTimeAbility ability)
		{
			_fillController.Deplete(ability.Duration); // Al activarse, dreno la barra según la duración de la habilidad
		}

		private void OnAbilityChargeChange(float value, BulletTimeAbility ability)
		{
			if (!ability.IsAbilityActive)
			{
				_fillController.Fill = value; // Establezco el valor al progreso de la habilidad
			}
		}

		private IEnumerator Wait(float seconds, Action callback)
		{
			if (callback == null) yield break;
			if (seconds <= 0)
			{
				callback?.Invoke();
				yield break;
			}

			yield return new WaitForSeconds(seconds);
			callback?.Invoke();
		}
	}
}