﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameScrollComponent : MonoBehaviour
{
	[SerializeField]
	private Transform _childContent = null;
	[SerializeField]
	private float _warpDistance = 0f;
	[SerializeField]
	private float _speed = 2f;
	[Header("Events")]
	[SerializeField]
	private UnityEvent _onWarp = null;

	private void Awake()
	{
		if (_childContent == null)
		{
			_childContent = transform.childCount == 0
			? new GameObject("Content").transform
			: transform.GetChild(0);

			_childContent.parent = transform;
		}
	}

	private void Update()
	{
		var position = transform.position;
		position.y -= Time.deltaTime * _speed;
		transform.position = position;

		if (Mathf.Abs(position.y) > _warpDistance)
			Warp();

	}

	public void Warp()
	{
		_childContent.localPosition += transform.localPosition;
		transform.localPosition = Vector3.zero;

		_onWarp?.Invoke();
	}
}
