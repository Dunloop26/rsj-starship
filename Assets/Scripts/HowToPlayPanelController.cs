﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HowToPlayPanelController : MonoBehaviour
{
	[Header("Dependencies")]
	[SerializeField] private Image _image = null;
	[SerializeField] private TextMeshProUGUI _paragraph = null;

	public void SetImage(Sprite sprite)
	{
		if (_image == null) return;
		_image.sprite = sprite;
	}

	public void SetText(string contents)
	{
		if (_paragraph == null) return;
		_paragraph.text = contents;
	}
}