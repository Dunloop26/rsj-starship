﻿using System.Collections.Generic;
using UnityEngine;

public class RoutineBuilder : MonoBehaviour
{
  public enum RoutineType
  {
    Wait,
    Waypoint,
    ScaleTransition,
  }

  [System.Serializable]
  public class RoutineDefinition
  {
    [Header("Settings")]
    [SerializeField] private float _duration = 0f;
    [SerializeField] private RoutineType _type = RoutineType.Wait;
    [SerializeField] private Vector3[] _routineParams = null;
    [Header("Extra")]
    [SerializeField] private float _waypointInBetweenDuration = 0f;
    [SerializeField] private bool _waypointUseRelativePositions = false;
    public RoutineType Type { get => _type; set => _type = value; }
    public Vector3[] RoutineParams { get => _routineParams; set => _routineParams = value; }
    public float Duration { get => _duration; set => _duration = value; }
    public float WaypointInBetweenDuration { get => _waypointInBetweenDuration; set => _waypointInBetweenDuration = value; }
    public bool WaypointUseRelativePositions { get => _waypointUseRelativePositions; set => _waypointUseRelativePositions = value; }
  }

  [Header("Settings")]
  [SerializeField] private RoutineDefinition[] _definitions = null;
  [Space]
  [SerializeField] private Transform _target = null;
  [Space]
  [SerializeField] private bool _reversed = false;
  public Routine[] GetRoutines(bool? reversed = null)
  {
    return BuildRoutines(_definitions, reversed ?? _reversed);
  }

  private Routine[] BuildRoutines(RoutineDefinition[] definitions, bool reversed)
  {
    var output = new List<Routine>();

    int length = definitions.Length;
    Routine currentRoutine = null;
    RoutineDefinition current;
    for (int i = 0; i < length; i++)
    {
      current = definitions[reversed ? (length - 1) - i : i];
      switch (current.Type)
      {
        case RoutineType.Wait:
          currentRoutine = new WaitRoutine(current.Duration);
          break;
        case RoutineType.Waypoint:
          currentRoutine = new WaypointRoutine(_target,
          current.RoutineParams,
          current.WaypointInBetweenDuration,
          current.Duration).SetUseRelativePositions(current.WaypointUseRelativePositions);
          break;
        case RoutineType.ScaleTransition:
          currentRoutine = new ScaleRoutine(_target,
          current.RoutineParams,
          current.Duration);
          break;
        default:
          continue;
      }

      if (currentRoutine != null)
        output.Add(currentRoutine);
    }

    return output.ToArray();
  }
}