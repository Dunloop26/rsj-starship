﻿using System.Collections;
using Dunas.Events;
using UnityEngine;
using UnityEngine.Events;

namespace Game
{
	public class GameScoreCounter : MonoBehaviour
	{
		[Header("Settings")]
		[SerializeField] private int _scorePerSecond = 100;
		[Header("Events")]
		[SerializeField] private FloatEvent _scoreUpdate = null;
		[SerializeField] private UnityEvent _scoreReset = null;
		private int _score = 0;
		private WaitForSeconds _eachHalfSecond;
		public int ScorePerSecond { get => _scorePerSecond; set => _scorePerSecond = value; }
		public int Score { get => _score; set => _score = value; }

		private void Awake()
		{
			_eachHalfSecond = new WaitForSeconds(0.5f);
		}

		public void StartCounter()
		{
			EndCounter();
			StartCoroutine(Counter());
		}

		public void ResetScore()
		{
			Score = 0;
			_scoreReset?.Invoke();
		}

		public void AddScore(int scorePoints)
		{
			Score += scorePoints;
			_scoreUpdate?.Invoke(Score);
		}

		public void EndCounter()
		{
			StopCoroutine("Counter");
		}

		private IEnumerator Counter()
		{
			int halfScore = ScorePerSecond / 2;
			while (enabled)
			{
				yield return _eachHalfSecond;
				AddScore(halfScore);
			}

			yield return null;
		}
	}
}