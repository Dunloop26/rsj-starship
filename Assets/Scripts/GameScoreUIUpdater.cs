﻿using Dunas.Events;
using TMPro;
using UnityEngine;

namespace Game
{

	public class GameScoreUIUpdater : MonoBehaviour
	{
		[Header("Dependencies")]
		[SerializeField] private GameScoreCounter _counter = null;
		[Header("Event")]
		[SerializeField] private StringEvent _scoreUpdate = null;
		[Header("Settings")]
		[SerializeField] private string _format = string.Empty;

		public void UpdateScoreUI()
		{
			_scoreUpdate?.Invoke(_counter.Score.ToString(_format));
		}
	}
}