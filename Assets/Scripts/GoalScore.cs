﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GoalScore : MonoBehaviour
{
  [System.Serializable] private class FloatEvent : UnityEvent<float> { }

  [Header("Settings")]
  [SerializeField] private int _startingGoals = 15;
  [SerializeField] private int _pointPerScore = -1;

  [Header("Bounds")]
  [SerializeField] private Vector2Int _bounds = new Vector2Int(0, 15);

  [Header("Events")]
  [SerializeField] private FloatEvent _currentScoreValue = null;
  [SerializeField] private UnityEvent _onMinBoundReach = null;
  [SerializeField] private UnityEvent _onMaxBoundReach = null;

  private int _currentGoals;
  public int CurrentGoals { get => _currentGoals; private set => _currentGoals = value; }
  public Vector2Int Bounds { get => _bounds; set => _bounds = value; }

  private void Start()
  {
    _currentScoreValue?.Invoke(_startingGoals);
    CurrentGoals = _startingGoals;
  }

  public void AddScore()
  {
    CurrentGoals += _pointPerScore;
		if (CurrentGoals <= _bounds[0]) _onMinBoundReach?.Invoke();
		if (CurrentGoals >= _bounds[1]) _onMaxBoundReach?.Invoke();
    
    CurrentGoals = Mathf.Clamp(CurrentGoals, Bounds[0], Bounds[1]);


    _currentScoreValue?.Invoke(CurrentGoals);
  }

  public void ResetScore()
  {
    CurrentGoals = _startingGoals;
    _currentScoreValue?.Invoke(CurrentGoals);
  }
}
