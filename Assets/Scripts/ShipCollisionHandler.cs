﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipCollisionHandler : MonoBehaviour
{
	[Header("Settings")]
	[SerializeField] private TriggerNotifier[] _notifiers = null;

	private void Awake()
	{
		int length = _notifiers.Length;
		for (int i = 0; i < length; i++)
		{
			if (_notifiers[i] == null) continue;
			_notifiers[i].TriggerEnter += OnShipTriggerEnter;
		}
	}
	private void OnDestroy()
	{
		int length = _notifiers.Length;
		for (int i = 0; i < length; i++)
		{
			if (_notifiers[i] == null) continue;
			_notifiers[i].TriggerEnter -= OnShipTriggerEnter;
		}
	}
	private void OnShipTriggerEnter(Collider other)
	{
		var asteroid = other.gameObject.GetComponent<Asteroid>();

		if (asteroid != null)
		{
			Vector2 pushDirection = (other.transform.position - transform.position).normalized;
			asteroid.Direction = pushDirection;
			asteroid.Speed *= 1.25f;
		}
	}
}
