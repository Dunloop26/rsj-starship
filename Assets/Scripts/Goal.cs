﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Goal : MonoBehaviour
{
  [SerializeField] private TriggerNotifier _notifier = null;
  [Header("Events")]
  [SerializeField] private UnityEvent _onAsteroidOnGoal = null;

  private Collider _lastHit = null;

  private Transform _transform;

  private void Awake()
  {
    if (_notifier != null)
    {
      _notifier.TriggerEnter += OnGoalTriggerEnter;
    }

    _transform = transform;
  }

  private void OnDestroy()
  {
    if (_notifier != null)
    {
      _notifier.TriggerEnter += OnGoalTriggerEnter;
    }
  }

  private void OnGoalTriggerEnter(Collider other)
  {
    if (_lastHit != null && _lastHit == other) return;

    var asteroid = other.GetComponent<Asteroid>();
    if (asteroid != null)
    {
      _lastHit = other;
      asteroid.gameObject.SetActive(false);
      _onAsteroidOnGoal?.Invoke();
    }
  }
}
