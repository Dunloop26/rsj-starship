﻿using UnityEngine;

public class HowToPlayPanelData : MonoBehaviour
{
	[System.Serializable]
	public class DataDefinition
	{
		[Header("Settings")]
		[SerializeField] private Sprite _image = null;
		[SerializeField, Multiline()] private string _contents = string.Empty;
		public DataDefinition(Sprite image, string contents)
		{
			Image = image;
			Contents = contents;
		}
		public Sprite Image { get => _image; set => _image = value; }
		public string Contents { get => _contents; set => _contents = value; }
	}

	[Header("Dependencies")]
	[SerializeField] private HowToPlayPanelController _panel = null;
	[Header("Data")]
	[SerializeField] private DataDefinition[] _data = null;
	[Space]
	[SerializeField] private bool _displayFirstOnEnable = true;

	private int _currentIndex = -1;

	private void OnEnable()
	{
		if (!_displayFirstOnEnable) return;

		_currentIndex = 0;
		Display(_currentIndex);
	}

	public void ShowNext()
	{
		var nextIndex = (_currentIndex + 1) % _data.Length;
		_currentIndex = nextIndex;

		Display(nextIndex);
	}

	public void ShowPrevious()
	{
		var previousIndex = _currentIndex - 1;
		if (previousIndex < 0) previousIndex = _data.Length + previousIndex; // Sumo ya que previous index es negativo

		Debug.Log($"Current: {_currentIndex} || Previous: {previousIndex}", this);

		_currentIndex = previousIndex;
		Display(previousIndex);
	}

	public void Display(int dataIndex)
	{
		if (dataIndex < 0 || dataIndex >= _data.Length) return;

		var displayData = _data[dataIndex];
		if (displayData == null) return;

		SetDataDefinition(displayData);
	}

	private void SetDataDefinition(DataDefinition data)
	{
		if (_panel == null) return;
		_panel.SetImage(data.Image);
		_panel.SetText(data.Contents);
	}
}