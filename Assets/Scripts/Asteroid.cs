﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Dunas.Utils;

public class Asteroid : MonoBehaviour
{
	[SerializeField]
	private Vector2 _rotationSpeed = Vector2.up;
	[SerializeField]
	private float _speed = 1f;
	private Transform _transform;
	private Vector3 _direction;
	private Quaternion _rotation = Quaternion.Euler(1f, 0, 0);
	public float Speed { get => _speed; set => _speed = value; }
  public Vector3 Direction { get => _direction; set => _direction = value; }

  private void OnEnable()
	{
		_rotation = Quaternion.Euler(Random.insideUnitSphere * Random.Range(_rotationSpeed[0], _rotationSpeed[1]));
		StartCoroutine(Rotate());
	}

	private IEnumerator Rotate()
	{
		while (true)
		{
			if (_transform == null) _transform = transform;
			_transform.rotation *= _rotation;
			yield return null;
		}
	}

	private void OnDisable()
	{
		StopCoroutine("Rotate");
	}
	public void Move()
	{
		if (_transform == null) _transform = transform;
		_transform.position += (Vector3)_direction * Speed * Time.deltaTime;
	}
}
