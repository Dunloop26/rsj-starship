﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using DG.Tweening;
using UnityEngine.Events;
using Game;
using Game.Input;

public class Starship : MonoBehaviour
{
  [System.Serializable] private class FloatEvent : UnityEvent<float> { }
	[Header("Settings")]
	[SerializeField] private GameInputReceiver _inputReceiver = null;
  [Space]
  [SerializeField]
  private float _steerDrag = 1f;
  [SerializeField]
  private float _steerAcceleration = 1f;
  [SerializeField]
  private float _dragSpeed = 3f;
  [Space]
  [SerializeField]
  private float _dragLost = 1f;
  [SerializeField]
	private float _thrustForce = 1f;
  [Space]
  [SerializeField]
  private float _maxDragForce = 5f;
  [SerializeField]
  private float _maxSteerSpeed = 2f;
	[Header("Overdrive Settings")]
	[SerializeField] private float _steerMultiplier = 2f;
	[Header("Ship Settings")]

  [SerializeField]
  private float _shipWidth = 0.5f;
  private float _dragAmount = 1f;
  private float _steerAmount = 0f;
	private bool _thrusting = false;
  private float _steerAnimationValue = 0f;

  private float _direction = 0;
  private float _lastDirection = 0;
  [SerializeField]
  private Vector2 _steerLimits = new Vector2(-1, 1);
  [SerializeField]
  private Vector2 _dragLimit = new Vector2(-1, 1);

  [Header("Animation Events")]
  [SerializeField]
  private float _steerAnimationLength = 1f;
  [SerializeField]
  private float _counterRotationWeight = 1f;
  [SerializeField]
  private FloatEvent _steerOutput = null;

  [Header("Debug")]
  [SerializeField]
  private bool _debug = false;

	public bool Overdrive { get; set; }

	private void Awake()
	{
		if (_inputReceiver == null) _inputReceiver = GetComponent<GameInputReceiver>();
		// Si el input receiver sigue siendo null, retorno
		if (_inputReceiver == null)
		{
			Debug.LogWarning($"The input receiver has not been defined for this instance", this);
			return;
		}

		BindEvents();
	}
	private void OnDestroy()
	{
		if (_inputReceiver == null) return;
		UnbindEvents();
	}

	private void BindEvents()
	{
		_inputReceiver.SteerGameInput += OnSteerInputPerformed;
		_inputReceiver.ThrustGameInput += OnThrustInputPerformed;
	}
	private void UnbindEvents()
	{
		_inputReceiver.SteerGameInput -= OnSteerInputPerformed;
		_inputReceiver.ThrustGameInput -= OnThrustInputPerformed;
	}

	private void Update()
  {
    if (_direction != 0)
    {
			_steerAmount += Time.deltaTime * _steerAcceleration * _direction * (Overdrive ? _steerMultiplier : 1f);
      if (_direction == -Mathf.Sign(_steerAmount))
        _steerAmount += Time.deltaTime * _steerDrag * _direction;

      _steerAnimationValue += Time.deltaTime * _steerAnimationLength * _direction;

      if (_direction == -Mathf.Sign(_steerAnimationValue))
        _steerAnimationValue += Time.deltaTime * _counterRotationWeight * _direction;

    }
    else
    {
      if (_steerAmount != 0f)
      {
        float previousDirection = Mathf.Sign(_steerAmount);

				_steerAmount += Time.deltaTime * _steerDrag * -_lastDirection * (Overdrive ? _steerMultiplier : 1f);

        if (previousDirection != Mathf.Sign(_steerAmount))
        {
          _steerAmount = 0f;
        }
      }

      if (_steerAnimationValue != 0)
      {
        float previousAnimationSign = Mathf.Sign(_steerAnimationValue);
        _steerAnimationValue -= Time.deltaTime * _steerAnimationLength * previousAnimationSign;

        if (previousAnimationSign != Mathf.Sign(_steerAnimationValue))
        {
          _steerAnimationValue = 0f;
        }
      }
    }

    _steerAnimationValue = Mathf.Clamp(_steerAnimationValue, -1, 1);

    Move(_steerAmount);

		if (_thrusting) _dragAmount = AddThrust(_dragAmount);

    Drag(_dragAmount);

    _steerOutput?.Invoke(_steerAnimationValue);
  }

	private float AddThrust(float dragAmount)
  {
		dragAmount -= _thrustForce * Time.deltaTime;
		Debug.Log($"This is thrust :{dragAmount} {_thrustForce}", this);
    dragAmount = Mathf.Clamp(dragAmount, -_maxDragForce, _maxDragForce);
    return dragAmount;
  }

  private void Drag(float dragAmount)
  {
    float dragDistance = -dragAmount * Time.deltaTime;

    if (!OutOfDragLimits(transform.position.y + dragDistance))
      transform.position += Vector3.up * dragDistance;
    else dragAmount = 0f;

    Debug.DrawRay(transform.position, Vector3.up * dragDistance);

    dragAmount += _dragLost * Time.deltaTime;
    dragAmount = Mathf.Clamp(dragAmount, -_maxDragForce, _maxDragForce);
    _dragAmount = dragAmount;
  }

	private void OnSteerInputPerformed(float direction, GameInputReceiver receiver)
	{
    _lastDirection = _direction;
		_direction = direction;
  }

	private void OnThrustInputPerformed(bool thrust, GameInputReceiver receiver)
	{
		_thrusting = thrust;
  }

  private void Move(float steerDirection)
  {
    float steerDistance = steerDirection * Time.deltaTime;
    if (OutOfSteerLimits(transform.position.x + steerDistance))
    {
      _steerAmount = 0f;
      return;
    }

    transform.Translate(Vector3.right * steerDistance);
  }

  private bool OutOfSteerLimits(float value)
  {
    return (value < _steerLimits[0] + _shipWidth || value > _steerLimits[1] - _shipWidth);
  }

  private bool OutOfDragLimits(float value)
  {
    return (value < _dragLimit[0] + _shipWidth || value > _dragLimit[1] - _shipWidth);
  }

  private void OnDrawGizmos()
  {
    if (!_debug) return;

    Gizmos.color = Color.red;

    Vector3 min, max;
    min.z = max.z = transform.position.z;
    min.y = max.y = transform.position.y - 50;
    min.x = _steerLimits[0];
    max.x = _steerLimits[1];

    Vector3[] dragLimits =
    {
      transform.position,
      transform.position,
    };

    dragLimits[0].y = _dragLimit[0];
    dragLimits[1].y = _dragLimit[1];

    dragLimits[0].x -= 50;
    dragLimits[1].x -= 50;


    Gizmos.DrawRay(min, Vector2.up * 100);
    Gizmos.DrawRay(max, Vector2.up * 100);
    Gizmos.DrawRay(dragLimits[0], Vector2.right * 100);
    Gizmos.DrawRay(dragLimits[1], Vector2.right * 100);

    Gizmos.DrawWireSphere(transform.position, _shipWidth);
  }
}