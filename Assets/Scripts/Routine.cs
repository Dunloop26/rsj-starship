﻿using Dunas.Coroutine;
using UnityEngine;

public interface IRoutine
{
  float Duration {get;set;}
  void StartRoutine();
  void EndRoutine();
}

[System.Serializable]
public abstract class Routine : IRoutine
{
  public abstract void StartRoutine();
  public abstract void EndRoutine();
  public abstract float Duration { get; set; }
}
