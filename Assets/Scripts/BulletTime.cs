﻿using System.Collections;
using UnityEngine;

namespace Game
{
	public class BulletTime : MonoBehaviour
	{
		[Header("Time stretch")]
		[SerializeField, Range(0.05f, 1f)] private float _timeStretch = 1f;
		[Header("Lerp Options")]
		[SerializeField] private float _lerpDuration = 1f;

#if UNITY_EDITOR
		private float _lastUpdateTimeStretch = 0f;

		private void Update()
		{
			if (_lastUpdateTimeStretch != _timeStretch)
			{
				Time.timeScale = _timeStretch;
				_lastUpdateTimeStretch = _timeStretch;
			}
		}
#endif

		private bool _coLerpTime = false;

		public float TimeStretch
		{
			get => _timeStretch;
			set
			{
				value = Mathf.Clamp(value, 0.05f, 1f);
				_timeStretch = value;
				Time.timeScale = _timeStretch;
			}
		}

		public void LerpTo(float target)
		{
			StartCoroutine(LerpTime(target, _lerpDuration));
		}

		private IEnumerator LerpTime(float to, float seconds)
		{
			if (_coLerpTime) yield break;
			_coLerpTime = true;

			float elapsed = 0f;
			float percent = 0f;

			float startTime = TimeStretch;

			while (elapsed < seconds)
			{
				percent = elapsed / seconds;
				TimeStretch = Mathf.Lerp(startTime, to, percent);
				elapsed += Time.deltaTime;
				yield return null;
			}

			TimeStretch = to;
			_coLerpTime = false;
		}
	}
}