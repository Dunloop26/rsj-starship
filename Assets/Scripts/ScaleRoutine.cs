﻿using System;
using DG.Tweening;
using UnityEngine;

public class ScaleRoutine : Routine
{
  public ScaleRoutine(Transform target, Vector3[] scaleTransitions, float duration)
  {
    Target = target;
    ScaleTransitions = scaleTransitions;
    Duration = duration;
  }
  public Transform Target { get; }
  public Vector3[] ScaleTransitions { get; }

  private Vector3 _startScale;
  public override float Duration { get; set; }

  public bool ResetOnEnd { get; set; }
  public bool Looping { get; set; }

  private Sequence _sequence;

  public ScaleRoutine SetResetOnEnd(bool value)
  {
    ResetOnEnd = value;
    return this;
  }

  public ScaleRoutine SetDuration(float duration)
  {
    Duration = duration;
    return this;
  }

  public ScaleRoutine SetLooping(bool looping)
  {
    Looping = looping;
    return this;
  }
  public override void StartRoutine()
  {
    _startScale = Target.localScale;

    var startTween = Target.DOScale(ScaleTransitions[0], 0.3f);
    startTween.OnComplete(() =>
   {
     _sequence = BuildSequence(ScaleTransitions);
     _sequence.Play();
   }
    );
  }

  private Sequence BuildSequence(Vector3[] scaleTransitions)
  {
    Sequence seq = DOTween.Sequence();

    int length = scaleTransitions.Length;

    float duration = length != 0 ? Duration / (float)length : 1f;
    for (int i = 1; i < length; i++)
    {
      seq.Append(Target.DOScale(scaleTransitions[i], duration));
    }

    if (Looping) seq.SetLoops(-1).SetAutoKill(false);
    return seq;
  }

  public override void EndRoutine()
  {
    _sequence.Pause();
    _sequence.Kill();
  }
}