﻿using System.Collections;
using System.Collections.Generic;
using Dunas.Pooling;
using UnityEngine;
using UnityEngine.InputSystem;

public class AsteroidSpawner : MonoBehaviour
{
  private static readonly float MAX_DISTANCE = 20f;
  [Header("Settings")]
  [SerializeField]
  private Transform _min = null;
  [SerializeField]
  private Transform _max = null;
  [Header("Spawn Settings")]
  [SerializeField]
  private float _secondsPerSpawn = 5f;
  [SerializeField]
  private int _totalInlineAsteroid = 3;
  [SerializeField]
  private Vector2 _asteroidSpeed = Vector2.up;
  [SerializeField]
  private float _baseSpeed = 1f;
	[SerializeField] private float _increasePerSecond = 0.2F;
  [SerializeField]
  private string _asteroidPoolID = "asteroid";
  [SerializeField]
  private Transform _parent = null;
  [Space]
  [SerializeField]
  private int _currentAsteroidsForSpawn = 1;

  public Transform Max { get => _max; set => _max = value; }
  public Transform Min { get => _min; set => _min = value; }
  public float SecondsPerSpawn { get => _secondsPerSpawn; set => _secondsPerSpawn = value; }
  public int CurrentAsteroidsForSpawn { get => _currentAsteroidsForSpawn; set => _currentAsteroidsForSpawn = value; }
	public float BaseSpeed { get => _baseSpeed; set => _baseSpeed = value; }

  private float _nextTimeSpawn = 0f;
  private List<Asteroid> _asteroids = new List<Asteroid>();

  private Pool _asteroidPool = null;

  public bool EnableSpawning { get; set; } = false;

  private void Awake()
  {
    if (_parent == null) _parent = transform;

    if (string.IsNullOrEmpty(_asteroidPoolID))
    {
      Debug.LogError($"The asteroid pool ID is null or empty", this);
      return;
    }

    _asteroidPool = PoolManager.GetPool(_asteroidPoolID);
  }
  // TODO: Crear ratio de spawn dependiendo del progreso del jugador -> pocos al inicio, la mayoría al final

  private void OnEnable()
  {
    _nextTimeSpawn = Time.time + _secondsPerSpawn;
  }

  private void Update()
  {
    if (EnableSpawning)
    {
      if (Time.time > _nextTimeSpawn)
      {
        _nextTimeSpawn = Time.time + _secondsPerSpawn;
        Spawn(Random.Range(0, _currentAsteroidsForSpawn + 1));
      }
			_baseSpeed += Time.deltaTime * _increasePerSecond;
    }

    HandleDespawn();
    PushAsteroids();
  }

  private void HandleDespawn()
  {
    if (_asteroids == null) return;

    int length = _asteroids.Count;
    Transform current;
    float distance;
    for (int i = length - 1; i >= 0; i--)
    {
      current = _asteroids[i]?.transform ?? null;
      if (current == null) continue;

      var asteroid = current.GetComponent<Asteroid>();
      if (current.gameObject.activeInHierarchy)
      {
        distance = Mathf.Abs(transform.position.y - current.position.y);

        if (distance > MAX_DISTANCE)
        {
          if (asteroid != null)
          {
            _asteroids.Remove(asteroid);
          }

          current.gameObject.SetActive(false);
        }
      }
      else
      {
        _asteroids.Remove(asteroid);
      }
    }
  }

  private void PushAsteroids()
  {
    int length = _asteroids.Count;
    Asteroid current;
    for (int i = length - 1; i >= 0; i--)
    {
      current = _asteroids[i];
      if (current == null || !current.gameObject.activeInHierarchy) return;

      current.Move();
    }
  }

  private Vector3[] GetInlinePositions()
  {
    if (Min == null || Max == null) return new Vector3[0];

    Vector3 minPosition = Min.position;
    Vector3 maxPosition = Max.position;

    minPosition.y = maxPosition.y = transform.position.y;

    Vector3[] outputPositions = new Vector3[_totalInlineAsteroid];
    for (int i = 0; i < _totalInlineAsteroid; i++)
    {
      float fraction = i / (float)(_totalInlineAsteroid - 1);
      outputPositions[i] = Vector3.Lerp(minPosition, maxPosition, fraction);
    }

    return outputPositions;
  }

  public void Spawn(int number = -1)
  {
    if (_asteroidPool == null)
    {
      Debug.LogError($"The asteroid pool reference is null", this);
      return;
    }

    if (Keyboard.current.pKey.isPressed)
    {
      Debug.Log("Debug", this);
    }
    if (number <= 0)
      number = Random.Range(0, CurrentAsteroidsForSpawn) + 1;

    var positions = ChooseRandom(GetInlinePositions(), number);

    // TODO: Spawnear los asteroides con un radio de aleatoriedad en su posición para que se vean más naturales
    for (int i = 0; i < number; i++)
    {
      Vector3 randomUnit = Random.insideUnitSphere;
      randomUnit.z = 0f;
      var currentPosition = positions[i] + randomUnit * 0.5f;

      var poolItem = _asteroidPool.GetFromPool();
      if (poolItem == null)
      {
        Debug.LogError($"The retrieved pool item is null", this);
        return;
      }

      var asteroid = poolItem.GetComponent<Asteroid>();
      if (asteroid != null)
      {
        asteroid.transform.position = currentPosition;
        asteroid.Speed = _baseSpeed + Random.Range(_asteroidSpeed[0], _asteroidSpeed[1]);
        asteroid.Direction = Vector2.down;

        if (!_asteroids.Contains(asteroid))
          _asteroids.Add(asteroid);
      }
    }
  }

  private Vector3[] ChooseRandom(Vector3[] inlinePositions, int number)
  {
    if (number <= 0) return new Vector3[0];
    if (number >= inlinePositions.Length) return inlinePositions;

    List<int> randomIndexes = new List<int>();
    int maxAttempts = 1000;
    int attempts = 0;

    while (randomIndexes.Count < number || attempts > maxAttempts)
    {
      attempts++;
      int random = Random.Range(0, inlinePositions.Length);

      if (randomIndexes.Contains(random)) continue;
      randomIndexes.Add(random);
    }

    Vector3[] output = new Vector3[number];

    for (int i = 0; i < number; i++)
    {
      output[i] = inlinePositions[randomIndexes[i]];
    }

    return output;
  }

  void OnDrawGizmos()
  {
    if (Min == null || Max == null) return;

    Gizmos.color = Color.yellow;

    var inlinePositions = GetInlinePositions();

    int length = inlinePositions.Length;
    for (int i = 0; i < _totalInlineAsteroid; i++)
    {
      Gizmos.DrawWireSphere(inlinePositions[i], 0.25f);
    }
  }
}
