﻿public class WaitRoutine : Routine
{
  public WaitRoutine(float duration)
  {
    Duration = duration;
  }
  public override float Duration { get; set; }
  public override void StartRoutine()
  {
    // No setup required
  }
  public override void EndRoutine()
  {
    // No setup required
  }
}