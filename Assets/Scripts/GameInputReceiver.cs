﻿using UnityEngine;

namespace Game.Input
{
	public class GameInputReceiver : MonoBehaviour
	{
		private float steerDirection;
		private bool thrustInput;

		public float SteerDirection
		{
			get => steerDirection;
			set
			{
				steerDirection = EnableInput ? value : 0f;
				SteerGameInput?.Invoke(steerDirection, this);
			}
		}
		public bool ThrustInput
		{
			get => thrustInput;
			set
			{
				thrustInput = EnableInput && value;
				ThrustGameInput?.Invoke(thrustInput, this);
			}
		}
		public bool EnableInput { get; set; }

		public delegate void SteerDelegate(float steerDirection, GameInputReceiver receiver);
		public delegate void ThrustDelegate(bool thrustInput, GameInputReceiver receiver);

		public event SteerDelegate SteerGameInput;
		public event ThrustDelegate ThrustGameInput;
	}
}