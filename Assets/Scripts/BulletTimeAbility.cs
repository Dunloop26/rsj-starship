﻿using System;
using System.Collections;
using Game.Input;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

namespace Game
{
	public class BulletTimeAbility : MonoBehaviour
	{
		[Header("Dependencies")]
		[SerializeField] private BulletTime _bulletTimeController = null;
		[SerializeField] private GameInputReceiver _inputReceiver = null;

		[Header("Control Values")]
		[SerializeField] private float _thrustBoost = 1f;
		[SerializeField] private float _thrustCooling = 1f;
		[Space]
		[SerializeField] private float _duration = 3f;
		[Header("Ability Settings")]
		[SerializeField] private float _slowdown = 0.75f;
		[Header("Events")]
		[SerializeField] private UnityEvent _abilityActivated = null;
		[SerializeField] private UnityEvent _abilityDeactivated = null;

		public delegate void AbilityFloatDelegate(float value, BulletTimeAbility ability);
		public delegate void AbilityDelegate(BulletTimeAbility ability);
		public event AbilityFloatDelegate ChargeChange;
		public event AbilityDelegate Activated;
		public event AbilityDelegate Deactivated;
		public float AbilityCharge => _abilityCharge;
		private bool _thrusting = false;
		private float _abilityCharge = 0f;

		private float _nextTimeCooldown = 0f;

		private bool _isAbilityActive = false;
		public bool IsAbilityActive { get => _isAbilityActive; private set => _isAbilityActive = value; }
		public bool IsThrusting => _inputReceiver?.ThrustInput ?? false;
		public float Slowdown { get => _slowdown; set => _slowdown = value; }
		public float Duration { get => _duration; set => _duration = value; }

		private void Start()
		{
			if (_inputReceiver == null) _inputReceiver = GetComponent<GameInputReceiver>();
		}

		private void Update()
		{
			if (!IsAbilityActive)
			{
				ChargeAbility();

				if (AbilityCharge >= 1f)
				{
					IsAbilityActive = true;
					ActivateAbility();
				}
			}
			else
			{
				if (IsAbilityOver())
					DeactivateAbility();
			}
		}

		public void ActivateAbility()
		{
			_nextTimeCooldown = Time.time + _duration;
			// Detengo la corutina en ejecución para empezar una nueva
			StopCoroutine("LerpBulletTime");
			StartCoroutine(LerpBulletTime(Slowdown, 0.25f * _duration));

			Activated?.Invoke(this);
			_abilityActivated?.Invoke();
		}

		private IEnumerator LerpBulletTime(float slowdown, float lerpDuration)
		{
			if (_bulletTimeController == null)
			{
				Debug.LogError("The bullet time is not defined for this operation");
				yield break;
			}

			float start = _bulletTimeController.TimeStretch;
			float elapsed = 0f;
			float percent = 0f;

			do
			{
				elapsed += Time.deltaTime;
				percent = elapsed / lerpDuration;

				_bulletTimeController.TimeStretch = Mathf.Lerp(start, slowdown, percent);
				yield return null;
			} while (elapsed < lerpDuration);

			_bulletTimeController.TimeStretch = slowdown;
		}

		public void DeactivateAbility()
		{
			_abilityCharge = 0f;
			IsAbilityActive = false;

			StopCoroutine("LerpBulletTime");
			StartCoroutine(LerpBulletTime(1f, 0.25f * _duration));

			Deactivated?.Invoke(this);
			_abilityDeactivated?.Invoke();
		}

		private bool IsAbilityOver()
		{
			return Time.time > _nextTimeCooldown;
		}

		private void ChargeAbility()
		{
			if (IsThrusting)
			{
				_abilityCharge += _thrustBoost * Time.deltaTime;
			}
			else
			{
				_abilityCharge -= _thrustCooling * Time.deltaTime;
			}

			_abilityCharge = Mathf.Clamp01(_abilityCharge);

			ChargeChange?.Invoke(_abilityCharge, this);
		}
	}
}