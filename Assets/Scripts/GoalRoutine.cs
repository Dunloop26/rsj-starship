﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.InputSystem;
using System;

namespace Game
{
  public class GoalRoutine : MonoBehaviour
  {
    [Header("Routines")]
    [SerializeField] private RoutineBuilder _routineBuilder = null;
    [Space]
    [SerializeField] private Transform _target = null;

    private Routine[] _runningRoutines;

    private bool _cancelRoutine = false;

    private bool _coStartRoutine;

    private Vector3 _defaultScale;
    private Vector3 _defaultPosition;

    private void Awake()
    {
      _defaultScale = _target.localScale;
      _defaultPosition = _target.position;
    }

    public void SetToDefaultTransform()
    {
      if (_target == null) return;
      StartCoroutine(SetToDefault());
    }

    public IEnumerator SetToDefault()
    {
      Vector3 currentScale = _target.localScale;
      Vector3 currentLocation = _target.position;

      float elapsed = 0f;
      float percent = 0f;
      const float duration = 0.25f;
      while (elapsed < duration)
      {
        _target.position = Vector3.Lerp(currentLocation, _defaultPosition, percent);
        _target.localScale = Vector3.Lerp(currentScale, _defaultScale, percent);

        percent = elapsed / duration;
        yield return null;
        elapsed += Time.deltaTime;
      }
    }
    public void StartRoutineSequence()
    {
      SetToDefaultTransform();
      _cancelRoutine = false;
      _runningRoutines = _routineBuilder.GetRoutines();
      StartCoroutine(PlayRoutine(_runningRoutines));
    }
    public void StopRoutineSequence()
    {
      _cancelRoutine = true;
    }

    private IEnumerator PlayRoutine(Routine[] routines)
    {
      if (_coStartRoutine) yield break;
      _coStartRoutine = true;

      int length = routines.Length;
      Routine current;

      for (int i = 0; i < length; i++)
      {
        current = routines[i];
        current.StartRoutine();
        float elapsed = 0f;
        float duration = current.Duration;
        while (elapsed < duration)
        {
          if (_cancelRoutine) break;
          elapsed += Time.deltaTime;
          yield return null;
        }

        current.EndRoutine();
        if (_cancelRoutine) break;
      }

      _cancelRoutine = false;

      _coStartRoutine = false;
    }
  }
}