﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;

public class TimeSpanChecker : MonoBehaviour
{

	public class DateTimeWrapper
	{
		public DateTimeWrapper(DateTime date)
		{
			year = date.Year;
			month = date.Month;
			day = date.Day;
			hour = date.Hour;
			minute = date.Minute;
			second = date.Second;
		}

		public int year;
		public int month;
		public int day;
		public int hour;
		public int minute;
		public int second;

		public DateTime GetAsDateTime()
		{
			DateTime output = new DateTime(year, month, day, hour, minute, second);
			return output;
		}
	}

	public struct TimeSpanCheckerComparationParams
	{
		public TimeSpanCheckerComparationParams(int day, int hour, int minute)
		{
			Day = day;
			Hour = hour;
			Minute = minute;
		}

		public TimeSpan GetAsTimeSpan()
		{
			return new TimeSpan(Day, Hour, Minute, 0);
		}

		public static implicit operator TimeSpanCheckerComparationParams(TimeSpanChecker timeSpanChecker)
		{
			return new TimeSpanCheckerComparationParams(timeSpanChecker.Days, timeSpanChecker.Hours, timeSpanChecker.Minutes);
		}

		public int Day { get; }
		public int Hour { get; }
		public int Minute { get; }
	}

	[SerializeField] private string _timeSpanDocPath;
	[Header("Span Settings")]
	[Range(0, 365)]
	[SerializeField] private int _days;
	[Range(0, 24)]
	[SerializeField] private int _hours;
	[Range(0, 60)]
	[SerializeField] private int _minutes;
	public string TimeSpanDocPath { get => _timeSpanDocPath; set => _timeSpanDocPath = value; }
	public int Days { get => _days; set => _days = value; }
	public int Hours { get => _hours; set => _hours = value; }
	public int Minutes { get => _minutes; set => _minutes = value; }

	[Header("Eventos")]
	[SerializeField] private UnityEvent _onTimeSpanSucceded;
	[SerializeField] private UnityEvent _onTimeSpanFailed;
	[SerializeField] private UnityEvent _onTimeSpanFileNotFound;


	[ContextMenu("Check Time Span")]
	public void EvaluateTimeSpan()
	{

		if (!File.Exists(_timeSpanDocPath))
		{
			if (_onTimeSpanFileNotFound != null) _onTimeSpanFileNotFound.Invoke();
			return;
		}
		else
		{
			// El archivo TimeSpan existe
			StartCoroutine(ReadFromPath(_timeSpanDocPath, (contents) =>
			{

				DateTime now = System.DateTime.Now;
				// Obtengo un wrapper de acuerdo a la lectura de los datos
				var wrapper = JsonUtility.FromJson<DateTimeWrapper>(contents);

				// Fecha obtenida del archivo
				var fileDateTime = wrapper.GetAsDateTime();

				if (IsInBoundTimeSpan(fileDateTime, now, this))
				{
					if(_onTimeSpanSucceded != null) _onTimeSpanSucceded.Invoke();
				}
				else
				{
					if(_onTimeSpanFailed != null) _onTimeSpanFailed.Invoke();
				}

			}));


		}

	}
	private IEnumerator ReadFromPath(string localPath, Action<string> readCallback, Action onErrorCallback = null)
	{
		if (readCallback == null)
		{
			Debug.LogWarning($"El Read Callback es nulo, la operación no continuará", this);
			yield break;
		}

		using (UnityWebRequest readRequest = new UnityWebRequest(localPath))
		{
			// Creo un download handler buffer para almacenar el texto
			DownloadHandler downloadHandler = new DownloadHandlerBuffer();
			readRequest.downloadHandler = downloadHandler;

			yield return readRequest.SendWebRequest();

			if (readRequest.isHttpError || readRequest.isNetworkError)
			{
				Debug.LogError($"ReadRequest Failed: {readRequest.error}", this);
				if (onErrorCallback != null) onErrorCallback();
				yield break;
			}
			else
			{
				// No hubo errores en el request
				readCallback(downloadHandler.text);
			}
		}
	}

	public bool IsInBoundTimeSpan(DateTime a, DateTime b, TimeSpanCheckerComparationParams comparisonParams)
	{
		TimeSpan diff = b - a;
		TimeSpan comparationTimeSpan = comparisonParams.GetAsTimeSpan();

		return diff <= comparationTimeSpan;
	}

	public void WriteCurrentDateTime()
	{

	}

	public void WriteDateTimeToPath(string path, DateTime date)
	{

	}

	public void WriteToPath(string path, string contents)
	{
	}
}
