﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Dunas.Events
{
	[System.Serializable] public class StringEvent : UnityEvent<string> { }
	[System.Serializable] public class StringArrayEvent : UnityEvent<string[]> { }
	[System.Serializable] public class IntEvent : UnityEvent<int> { }
	[System.Serializable] public class FloatEvent : UnityEvent<float> { }
	[System.Serializable] public class Vector3Event : UnityEvent<Vector3> {  }
	[System.Serializable] public class Vector2Event : UnityEvent<Vector2> {  }
	[System.Serializable] public class TransformEvent : UnityEvent<Transform> { }
	[System.Serializable] public class TransformArrayEvent : UnityEvent<Transform[]> { }
	[System.Serializable] public class GameObjectEvent : UnityEvent<GameObject> { }
	[System.Serializable] public class GameObjectArrayEvent : UnityEvent<GameObject[]> { }
	[System.Serializable] public class RaycastHitEvent  : UnityEvent<RaycastHit> {  }
	[System.Serializable] public class RaycastHitArrayEvent  : UnityEvent<RaycastHit[]> {  }
	[System.Serializable] public class SystemObjectEvent : UnityEvent<object> { }
	[System.Serializable] public class CameraEvent : UnityEvent<Camera> { }

	public static class EventExtensionMethods
	{
		public static void WrapInvoke<T>(this UnityEvent<T> unityEvent, T data)
		{
			// Invoco el evento si existe
			if (unityEvent != null) unityEvent.Invoke(data);
		}

		public static void WrapInvoke(this UnityEvent unityEvent)
		{
			// Invoco el evento si existe
			if (unityEvent != null) unityEvent.Invoke();
		}
	}
}
