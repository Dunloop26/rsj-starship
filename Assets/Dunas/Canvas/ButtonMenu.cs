﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class ButtonMenu : MonoBehaviour
{
  [Header("Menu")]
  [SerializeField] private Transform _menu;
  [Header("Settings")]
  [SerializeField] private bool _enableMenuOnAwake = false;
  [Header("Events")]
  [SerializeField] private UnityEvent _onMenuShow = new UnityEvent();
  [SerializeField] private UnityEvent _onMenuHide = new UnityEvent();

  public bool IsEnabled => _menu.gameObject.activeSelf;

  private Button _button;

  private bool IsMenuDefined()
  {
    // Si el menú no ha sido definido
    if (!_menu)
    {
      Debug.LogError($"The menu property isn't defined, can't do nothing : {gameObject.name}");
      return false;
    }

    return true;
  }

  // Start is called before the first frame update
  void Start()
  {
    // Si el menú no ha sido definido
    if (!IsMenuDefined()) return;

    // Defino el botón
    _button = GetComponent<Button>();

    // Defino la función de Activar y desactivar menú para el botón
    _button.onClick.AddListener(Toggle);

    // Activo el menú al inicializar el objeto
    _menu.gameObject.SetActive(_enableMenuOnAwake);

    // Manejo el disparo de los eventos
    HandleEventFiring();
  }

  public void HandleEventFiring()
  {
    // según el valor de la propiedad 'IsEnabled' disparo cierto evento
    if (IsEnabled) _onMenuShow.Invoke();
    else _onMenuHide.Invoke();
  }

  public void Toggle()
  {
    // Defino el valor de activación al menú al inverso de su valor anterior
    _menu.gameObject.SetActive(!IsEnabled);

    // Manejo el disparo de los eventos
    HandleEventFiring();
  }

  public void Show()
  {
    // Muestro el menú
    _menu.gameObject.SetActive(true);

    // Manejo el disparo de los eventos
    HandleEventFiring();
  }
  public void Hide()
  {
    // Oculto el menú
    _menu.gameObject.SetActive(false);

    // Manejo el disparo de los eventos
    HandleEventFiring();
  }

  public void Enable(bool value)
  {
    // Defino el valor al menú según el parámetro
    _menu.gameObject.SetActive(value);

    // Manejo el disparo de los eventos
    HandleEventFiring();
  }
}