﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Dunas.AssetData.Container
{
  public abstract class GenericContainer<T> : MonoBehaviour
  {
    [SerializeField] private T _value;
    public T Value
    {
      get
      {
        return _value;
      }
    }

    public abstract void GetValue();
  }
}
