﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Dunas.AssetData
{
  [CreateAssetMenu(menuName = "Dunas/AssetData/New String Data", fileName = "New String Data")]
  public class StringAssetData : GenericAssetData<string> { }
}
