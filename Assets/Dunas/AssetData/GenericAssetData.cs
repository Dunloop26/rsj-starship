﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Dunas.AssetData
{
  public abstract class GenericAssetData<T> : ScriptableObject, IValue<T>
  {
    [Header("DevDescription")]
    [Multiline(3)]
    [SerializeField] private string _devDescription;
    [SerializeField] private T _value;
    public T Value { get => _value; set => _value = value; }

    /// <summary>
    /// Convierte el valor del scriptable a su valor por defecto (Como null, por ejemplo)
    /// </summary>
    public void SetAsNull()
    {
      // Define el valor al valor por defecto de T
      _value = default(T);
    }

    /// <summary>
    /// Convierto el parametro contenedor, a su tipo T
    /// </summary>
    /// <param name="data">Contenedor T</param>
    public static implicit operator T(GenericAssetData<T> data)
    {
      return data.Value;
    }

  }
}
