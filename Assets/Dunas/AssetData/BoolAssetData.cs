﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Dunas.AssetData
{
  [CreateAssetMenu(menuName = "Dunas/AssetData/New Bool Data", fileName = "New Bool Data")]
  public class BoolAssetData : GenericAssetData<bool> { }
}
