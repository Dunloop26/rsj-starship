﻿using System;
using System.Collections.Generic;
using UnityEngine;

using Object = UnityEngine.Object;

namespace Dunas.AssetData
{
  [CreateAssetMenu(fileName = "ProjectAssetData", menuName = "Dunas/New Project Asset Data", order = 0)]
  public class ProjectAssetData : ScriptableObject
  {
    [System.Serializable]
    public class AssetDefinition
    {
      [Header("Definition Settings")]
      [SerializeField] private string _id = null;
      [SerializeField] private UnityEngine.Object _asset = null;

      public UnityEngine.Object Asset { get => _asset; set => _asset = value; }
      public string Id { get => _id; set => _id = value; }
    }
    [Header("Assets")]
    [SerializeField] private List<AssetDefinition> _assets = new List<AssetDefinition>();

    public Object GetAssetFromList(System.Predicate<AssetDefinition> match)
    {
      // Retorno el asset que coincida
      return _assets.Find(match).Asset;
    }

    public T GetAssetFromList<T>(System.Predicate<AssetDefinition> match) where T : Object
    {
      // Retorno el asset que coincida
      return _assets.Find(match).Asset as T;
    }
  }
}