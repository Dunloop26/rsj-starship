﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Object = UnityEngine.Object;

namespace Dunas.AssetData
{
  public class ProjectAssetLoader : MonoBehaviour
  {
    [Header("Assets Data")]
    [SerializeField] private ProjectAssetData _assetData = null;

    private static ProjectAssetLoader _instance;
    private static ProjectAssetLoader Instance
    {
      get
      {
        if (_instance == null) _instance = CreateInstance<ProjectAssetLoader>("ProjectAssetLoader");
        return _instance;
      }
    }

    public ProjectAssetData AssetData { get => _assetData; set => _assetData = value; }

    private static T CreateInstance<T>(string name) where T : MonoBehaviour
    {
      return new GameObject(name).AddComponent<T>();
    }

    public static Object GetAssetFromList(System.Predicate<ProjectAssetData.AssetDefinition> match)
    {
      return Instance.AssetData.GetAssetFromList(match);
    }
    public static T GetAssetFromList<T>(System.Predicate<ProjectAssetData.AssetDefinition> match) where T : Object
    {
      return Instance.AssetData.GetAssetFromList<T>(match);
    }
  }
}