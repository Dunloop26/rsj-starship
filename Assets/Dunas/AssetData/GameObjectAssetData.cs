﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Dunas.AssetData
{
  [CreateAssetMenu(menuName = "Dunas/AssetData/New GameObject Data", fileName = "New GameObject Data")]
  public class GameObjectAssetData : GenericAssetData<GameObject> { }
}
