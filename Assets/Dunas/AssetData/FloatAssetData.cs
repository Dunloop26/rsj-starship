
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Dunas.AssetData
{
  [CreateAssetMenu(menuName = "Dunas/AssetData/New Float Data", fileName = "New Float Data")]
  public class FloatAssetData : GenericAssetData<float> { }
}
