﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Dunas.AssetData
{
  [CreateAssetMenu(menuName = "Dunas/AssetData/New Transform Data", fileName = "New Transform Data")]
  public class TransformAssetData : GenericAssetData<Transform> { }
}
