﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Dunas.AssetData
{
  [CreateAssetMenu(menuName = "Dunas/AssetData/New Camera Data", fileName = "New Camera Data")]
  public class CameraAssetData : GenericAssetData<Camera> { }
}
