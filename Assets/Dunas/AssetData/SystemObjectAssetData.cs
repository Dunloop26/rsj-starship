﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Object = System.Object;

namespace Dunas.AssetData
{
  [CreateAssetMenu(menuName = "Dunas/AssetData/New SystemObject Data", fileName = "New SystemObject Data")]
  public class SystemObjectAssetData : GenericAssetData<Object> { }
}
