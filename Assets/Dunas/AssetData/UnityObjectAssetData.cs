﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Object = UnityEngine.Object;

namespace Dunas.AssetData
{
  [CreateAssetMenu(menuName = "Dunas/AssetData/New UnityObject Data", fileName = "New UnityObject Data")]
  public class UnityObjectAssetData : GenericAssetData<Object> { }
}
