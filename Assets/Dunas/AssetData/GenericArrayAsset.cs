﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Dunas.Modules
{

  public abstract class GenericArrayAsset<T> : ScriptableObject
  {
    [Header("Settings")]
    [Multiline]
    [SerializeField] private string _devDescription;
    [Space]
    [SerializeField] private T[] _data;

    public T[] Data
    {
      get => _data;
      set => _data = value;
    }
  }
}
