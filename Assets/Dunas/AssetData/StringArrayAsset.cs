﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Dunas.Modules
{
  [CreateAssetMenu(menuName = "Dunas/Modulos/String Array Asset", fileName = "New String Array Asset")]
  public class StringArrayAsset : GenericArrayAsset<string> { }

}
