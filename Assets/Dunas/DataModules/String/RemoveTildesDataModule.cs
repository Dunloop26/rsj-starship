﻿using UnityEngine;
using Dunas.Events;

namespace Dunas.DataModules.String
{
	public class RemoveTildesDataModule : MonoBehaviour
	{
		[Header("Action")]
		[SerializeField] private StringEvent _onStringProcess = new StringEvent();
		public StringEvent OnStringProcess => _onStringProcess;

		public void ProcessData(string data)
		{
			string output;

			output = Process(data);

			if (OnStringProcess == null) throw new System.NullReferenceException($"OnStringProcess no ha sido inicializado : {gameObject.name}");

			OnStringProcess.Invoke(output);
		}

		public static string Process(string data)
		{
			string output = data;

			output = output.Replace('á', 'a');
			output = output.Replace('é', 'e');
			output = output.Replace('í', 'i');
			output = output.Replace('ó', 'o');
			output = output.Replace('ú', 'u');

			output = output.Replace('Á', 'A');
			output = output.Replace('É', 'E');
			output = output.Replace('Í', 'I');
			output = output.Replace('Ó', 'O');
			output = output.Replace('Ú', 'U');

			return output;
		}
	}
}