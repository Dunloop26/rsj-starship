﻿using UnityEngine;
using Dunas.Events;

namespace Dunas.DataModules.String
{
	public class GetGameObjectName : MonoBehaviour
	{

		[Header("Action")]
		[SerializeField] private StringEvent _onStringProcess = new StringEvent();
		public StringEvent OnStringProcess => _onStringProcess;
		public void ProcessData(GameObject GO)
		{
			var name = Process(GO);

			if (OnStringProcess != null)
				OnStringProcess.Invoke(name);
		}

		public static string Process(GameObject GO)
		{
			return GO.name;
		}
	}
}
