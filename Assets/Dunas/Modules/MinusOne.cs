﻿using System.Collections;
using System.Collections.Generic;
using Dunas.Events;
using UnityEngine;

public class MinusOne : MonoBehaviour
{

  [Header("Settings")]
  [SerializeField] private FloatEvent _onProcess;

  public void Process(float arg)
  {
    // Devuelvo el resultado de 1 menos el argumento
    _onProcess.WrapInvoke(1 - arg);
  }
}
