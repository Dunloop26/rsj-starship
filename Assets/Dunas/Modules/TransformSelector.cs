﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Dunas.Events;

namespace Dunas.Modules
{
	/// <summary>
	/// Clase para seleccionar un transform de su jerarquía
	/// </summary>
	public class TransformSelector : MonoBehaviour
	{
		private static readonly float _destroyDelay = 0.001f;
		[Header("Events")]
		[SerializeField] private TransformEvent _onTransformSelected = new TransformEvent();
		[SerializeField] private List<Transform> _contents;

		// Inicia en -1 para que la primera llamada al ciclo de 0
		private int CycleCount { get; set; } = -1;

		/// <summary>
		/// Lista con todos los contenidos del objeto
		/// </summary>
		/// <value></value>
		public List<Transform> Contents { get => _contents; }

		private void OnEnable()
		{
			// Actualizo los contenidos
			UpdateContents();
		}

		/// <summary>
		/// Activa o desactiva todos los objetos registrados
		/// </summary>
		/// <param name="value">si 'true' activa los objetos, por lo contrario 'false' desactiva</param>
		public void SetActiveAll(bool value)
		{
			// Si la lista de contenidos existe
			if (_contents != null)
			{
				// Si la cuenta es mayor a 0
				if (_contents.Count > 0)
				{
					// Recorro cada elemento
					foreach (var element in _contents)
					{
						// Por cada elemento, defino el valor de activación o desactivación
						element.gameObject.SetActive(value);
					}
				}
			}
		}

		/// <summary>
		/// Activa el elemento dado
		/// </summary>
		/// <param name="target">Transform a activar</param>
		public void EnableTransform(Transform target)
		{
			// Si el target existe
			if (target != null)
				// Activo el objeto seleccionado
				target.gameObject.SetActive(true);
		}

		/// <summary>
		/// Desactiva el transform dado
		/// </summary>
		/// <param name="target">Transform a desactivar</param>
		public void DisableTransform(Transform target)
		{
			// Si el target existe
			if (target != null)
				// Desactivo el objeto seleccionado
				target.gameObject.SetActive(false);
		}

		/// <summary>
		/// Agrega todos los hijos como contenido al Selector
		/// </summary>
		public void UpdateContents()
		{
			// Obtengo los hijos en la jerarquia

			foreach (Transform child in transform)
			{
				// Añado el transform a la lista
				AddTransform(child);
			}
		}

		/// <summary>
		/// Añade referencias transform al selector
		/// </summary>
		/// <param name="target">Transform a añadir</param>
		public void AddTransform(Transform target)
		{
			// Si la lista no es nula
			if (_contents != null)
			{
				// Si no está contenido en la lista
				if (!_contents.Contains(target))
				{
					// Lo agrego a la lista
					_contents.Add(target);
				}
			}
		}

		/// <summary>
		/// Remueve el componente especificado por el nombre
		/// </summary>
		/// <param name="name">Nombre del Transform registrado a eliminar</param>
		public void RemoveTransform(string name)
		{
			// Si la lista no es nula
			if (_contents != null)
			{
				// Busco el objeto por el nombre dado
				var foundTransform = _contents.Find(tr => tr.name.Equals(name));

				// Si encontré un objeto
				if (foundTransform != null)
				{
					// Lo remuevo de la lista
					_contents.Remove(foundTransform);
				}
			}
		}

		/// <summary>
		/// Remueve el componente especificado por el nombre
		/// </summary>
		/// <param name="name">Nombre del Transform registrado a eliminar</param>
		public void RemoveDeleteTransform(string name)
		{
			// Si la lista no es nula
			if (_contents != null)
			{
				// Busco el objeto por el nombre dado
				var foundTransform = _contents.Find(tr => tr.name.Equals(name));

				// Si encontré un objeto
				if (foundTransform != null)
				{
					// Lo remuevo de la lista
					_contents.Remove(foundTransform);

					// Ejecuto el Destroy sobre el transform
					Destroy(foundTransform.gameObject, _destroyDelay);
				}
			}
		}


		/// <summary>
		/// Selecciona un elemento dado su nombre
		/// </summary>
		/// <param name="name">Nombre del elemento registrado</param>
		public void Select(string name)
		{
			// Genero la variable de salida
			Transform output = null;

			// Si la lista no es nula
			if (_contents != null)
			{
				// Si hay al menos un elemento en la lista
				if (_contents.Count > 0)
				{
					// Busco el elemento dado el nombre
					output = _contents.Find(tr => tr.name.Equals(name));
				}
			}

			// Ejecuto el evento de selección
			_onTransformSelected.WrapInvoke(output);
		}

		/// <summary>
		/// Obtiene un string del parámetro dado, si es nulo, no se ejecuta
		/// </summary>
		/// <param name="value">Valor del objeto a convertir</param>
		public void SelectTryParseString(object value)
		{
			// Convierto el valor del objeto a una cadena
			string name = (string)value;

			// Si el objeto convertido no es nulo ni está vacío
			if (!string.IsNullOrEmpty(name))
			{

				// Ejecuto el comando de selección
				Select(name);
			}
		}

		/// <summary>
		/// Limpia las referencias en la lista
		/// </summary>
		/// <param name="deleteContents">Limpia y elimina las referencias y los objetos en la escena</param>
		public void ClearList(bool deleteContents = false)
		{
			// Si la lista no es nula
			if (_contents != null)
			{
				// Si la lista contiene más de un elemento
				if (_contents.Count > 0)
				{
					// Obtengo la cuenta de elementos de la lista
					int count = _contents.Count;

					// Recorro cada elemento, de forma inversa
					for (int i = count - 1; i >= 0; i--)
					{
						// Obtengo el elemento actual
						var currentElement = _contents[i];

						// Elimino el elemento de la lista
						_contents.Remove(currentElement);

						// Si el elemento actual no es nulo
						if (currentElement != null && deleteContents)
						{
							// Elimino el objeto de la escena
							Destroy(currentElement, _destroyDelay);
						}
					}
				}
			}
		}

		/// <summary>
		/// Ciclo los elementos cada vez que se llame este método
		/// </summary>
		public void Cycle()
		{
			// Si la lista de contenidos existe
			if (_contents != null)
			{
				// Si al menos hay un contenido
				int count = _contents.Count;
				if (count > 0)
				{
					// Aumento la cuenta de ciclo
					CycleCount++;
					// Ciclo los contenidos
					var currentCycleTransform = _contents[CycleCount % count];

					// Ejecuto el evento
					_onTransformSelected.WrapInvoke(currentCycleTransform);
				}
			}
		}
	}
}
