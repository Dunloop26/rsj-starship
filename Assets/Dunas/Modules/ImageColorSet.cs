﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Dunas.Modules
{
	public class ImageColorSet : MonoBehaviour
	{
		[Header("Settings")]
		[SerializeField] private Image _image;
		[SerializeField] private bool _copyImageColorOnStart = true;

		[Header("Values")]
		[Tooltip("Cantidad del canal rojo (0 a 1)")]
		[SerializeField] private float _red;

		[Tooltip("Cantidad del canal verde (0 a 1)")]
		[SerializeField] private float _green;

		[Tooltip("Cantidad del canal azul (0 a 1)")]
		[SerializeField] private float _blue;

		[Tooltip("Cantidad del canal alpha")]
		[SerializeField] private float _alpha;


		public float Red { get => _red; set => _red = value; }
		public float Green { get => _green; set => _green = value; }
		public float Blue { get => _blue; set => _blue = value; }
		public float Alpha { get => _alpha; set => _alpha = value; }

		private float _storedRed = 0f, _storedGreen = 0f, _storedBlue = 0f, _storedAlpha = 0f;

		public void Start()
		{
			// Defino la imagen del componente
			if (_image == null) _image = GetComponent<Image>();

			// Si puedo copiar los colores de la imagen
			if (_copyImageColorOnStart)
			{
				// Copio los valores de color de la imagen
				Red = _image.color.r;
				Green = _image.color.g;
				Blue = _image.color.b;
				Alpha = _image.color.a;
			}

			// Actualizo el color
			UpdateImageColor(BuildColor());

			// Actualizo los valores almacenados
			UpdateStoredValues();
		}

		/// <summary>
		/// Construyo los colores usando las propiedades de la clase
		/// </summary>
		/// <returns>Color construido a partir de las propiedades</returns>
		private Color BuildColor()
		{
			return new Color(Red, Green, Blue, Alpha);
		}

		/// <summary>
		/// Actualizo el color de la imagen
		/// </summary>
		/// <param name="color">Color al que se va actualizar</param>
		public void UpdateImageColor(Color color)
		{
			// Si la imagen existe
			if (_image != null)
				_image.color = color;
		}

		private void Update()
		{
			// Compruebo si ha cambiado
			if (HasChanged())
			{
				// Actualizo los valores almacenados
				UpdateStoredValues();

				// Actualizo el color
				UpdateImageColor(BuildColor());
			}
		}

		/// <summary>
		/// ¿Ha cambiado alguno de los valores?
		/// </summary>
		/// <returns>'true' si ha cambiado alguno de los valores, 'false' si no ha cambiado ninguno</returns>
		private bool HasChanged()
		{
			return (!Red.Equals(_storedRed) ||
							!Blue.Equals(_storedRed) ||
							!Green.Equals(_storedGreen) ||
							!Alpha.Equals(_storedAlpha));
		}


		/// <summary>
		/// Actualiza los valores almacenados
		/// </summary>
		private void UpdateStoredValues()
		{
			_storedRed = Red;
			_storedBlue = Blue;
			_storedGreen = Green;
			_storedAlpha = Alpha;
		}
	}
}