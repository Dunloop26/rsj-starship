namespace Dunas.Modulos
{
	public interface IModuleProcessable
	{
		void Process();
	}
}