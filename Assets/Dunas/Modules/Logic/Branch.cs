﻿using System.Collections;
using System.Collections.Generic;
using Dunas.AssetData;
using Dunas.Events;
using UnityEngine;
using UnityEngine.Events;

namespace Dunas.Modules.Logic
{
  public class Branch : MonoBehaviour
  {
    [SerializeField] private BoolAssetData _condition;

    /// <summary>
    /// Condición a evaluar
    /// </summary>
    /// <value>BoolAssetData conteniendo un valor booleano</value>
    public BoolAssetData Condition { get => _condition; set => _condition = value; }

    [Header("Events")]
    [SerializeField] private UnityEvent _onEvaluationTrue;
    [SerializeField] private UnityEvent _onEvaluationFalse;

    /// <summary>
    /// Evalua la condición actual definida
    /// </summary>
    public void Evaluate()
    {
      // Obtengo la condición
      bool condition = Condition.Value;

      // Defino una referencia al evento en base a la condición
      var selectedBranch = (condition) ? _onEvaluationTrue : _onEvaluationFalse;

      // Ejecuto el evento seleccionado
      selectedBranch.WrapInvoke();
    }
  }
}
