﻿using System;
using System.Collections;
using System.Collections.Generic;
using Dunas.Events;
using UnityEngine;
using UnityEngine.Events;

using Object = UnityEngine.Object;

namespace Dunas.Modules
{
	public class MethodCaller : MonoBehaviour
	{
		[SerializeField] private Object _object;
		[SerializeField] private string _parseType;
		[Space]
		[SerializeField] private string _methodName;

		[Header("Event")]
		[SerializeField] private UnityEvent _onObjectValueSet = new UnityEvent();
		[SerializeField] private UnityEvent _onMethodCall = new UnityEvent();

		/// <summary>
		/// Objeto sobre el que se va a realizar la llamada
		/// </summary>
		/// <value></value>
		public Object ObjectValue
		{
			get => _object; set
			{
				_object = value;
				_onObjectValueSet.WrapInvoke();
			}
		}
		/// <summary>
		/// Nombre del método que se va a llamar
		/// </summary>
		/// <value>Nombre del método, debe de retornar void</value>
		public string MethodName { get => _methodName; set => _methodName = value; }

		/// <summary>
		/// Nombre al tipo convertido
		/// </summary>
		/// <value></value>
		public string ParseType { get => _parseType; set => _parseType = value; }

		/// <summary>
		/// Llama el método dado por los parámetros
		/// </summary>
		public void Call()
		{
			// Si el nombre del método a llamar no es nulo
			if (!string.IsNullOrEmpty(MethodName))
			{

				// Si el valor de objeto ha sido definido
				if (ObjectValue != null)
				{

					// Genero una variable de tipo igual a null
					System.Type type = null;

					// Si la cadena para la conversión del tipo es nula o está vacía
					if (string.IsNullOrEmpty(ParseType))
						// Obtengo el tipo directamente del objeto
						type = ObjectValue.GetType();
					else
					{
						// Obtengo el tipo desde la dirección al tipo registrado en el ensamblado en la cadena
						type = Type.GetType(ParseType);
					}

					// Si el tipo resultante no es nulo
					if (type != null)
					{

						// Obtengo el metodo por su nombre
						var method = type.GetMethod(MethodName);

						// Si el método no es nulo
						if (method != null)
						{
							// Invoco el método sin parámetros
							method.Invoke(ObjectValue, null);

							// Ejecuto el evento
							_onMethodCall.WrapInvoke();
						}
					}
				}
			}
		}
	}
}