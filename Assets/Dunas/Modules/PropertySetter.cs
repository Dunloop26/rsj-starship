﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Dunas.Events;

namespace Dunas.Modules
{
	/// <summary>
	/// Clase para escribir una propiedad directamente desde el inspector de Unity
	/// </summary>
	public class PropertySetter : MonoBehaviour
	{
		[Header("Settings")]

		///<summary>
		/// Objeto del cuál se va ejecutar la operación
		/// </summary>
		[SerializeField] private Object _object;

		/// <summary>
		///	Nombre de la propiedad sobre la qué escribir
		///</summary>
		[SerializeField] private string _propertyName;

		[SerializeField] private UnityEvent _onValueSet = new UnityEvent();

		/// <summary>
		/// Evento que se ejecuta después de definir el valor
		/// </summary>
		public UnityEvent OnValueSet => _onValueSet;

		/// <summary>
		/// Define el valor al objeto con el nombre de la propiedad especificada con los valores de la instancia
		/// </summary>
		/// <param name="value">Valor a definir sobre la propiedad</param>
		public void SetValue(object value)
		{
			// Defino el valor a la propiedad del objeto según el parámetro
			SetValueFrom(_object, _propertyName, value);

			// Ejecuto el evento
			OnValueSet.WrapInvoke();
		}

		/// <summary>
		/// Define la propiedad especificada a un Objeto de Unity
		/// </summary>
		/// <param name="value">Valor de Unity para escribir en la propiedad</param>
		public void SetUnityValue(Object value)
		{
			// Defino el valor a la propiedad del objeto según el parámetro
			SetValueFrom(_object, _propertyName, value);

			// Ejecuto el evento
			OnValueSet.WrapInvoke();
		}

		/// <summary>
		/// Define el valor al objeto con el nombre de la propiedad especificada
		/// </summary>
		/// <param name="target">Objeto sobre el cual se va a escribir el valor</param>
		/// <param name="propertyName">Nombre de la propiedad del objeto sobre la que se va escribir el valor</param>
		/// <param name="value">Valor actual que se va a escribir sobre la propiedad</param>
		public static void SetValueFrom(object target, string propertyName, object value)
		{
			// Si el objeto existe
			if (target != null)
			{
				// Obtengo el tipo del objeto
				var type = target.GetType();

				// Obtengo la propiedad del objeto
				var property = type.GetProperty(propertyName);

				// Si la propiedad existe
				if (property != null)
				{
					// Si no se puede escribir la propiedad
					if (!property.CanWrite)
					{
						Debug.LogError($"Can't write to property, probably missing a Set : {target} {propertyName}");
					}

					// Defino el valor de la propiedad segun el valor requerido
					property.SetValue(target, value);
				}
				else
				{
					// La propiedad no existe
					Debug.LogError($"The property '{propertyName}' on {type} object, doesn't exist");
				}
			}
			else
			{
				// El objeto no existe
				Debug.LogError($"The object to write the value on, doesn't exist : {propertyName}");
			}
		}

	}
}
