﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Dunas.Events;

namespace Dunas.Modules
{
	/// <summary>
	/// Clase para leer una propiedad directamente desde el inspector de Unity
	/// </summary>
	public class PropertyGetter : MonoBehaviour
	{
		[Header("Settings")]


		[SerializeField] private Object _object;
		[SerializeField] private string _propertyName;

		[Header("Events")]
		[SerializeField] private UnityEvent _onObjectValueSet = new UnityEvent();
		[SerializeField] private SystemObjectEvent _onValueGet = new SystemObjectEvent();

		/// <summary>
		/// Evento que se ejecuta después de obtener el valor
		/// </summary>
		public SystemObjectEvent OnValueGet => _onValueGet;

		/// <summary>
		/// Objeto del cuál se va a leer la operación
		/// </summary>
		/// <returns></returns>
		public Object Object
		{
			get => _object; set
			{
				// Defino el valor de objeto
				_object = value;

				// Ejecuto el evento
				_onObjectValueSet.WrapInvoke();
			}
		}
		/// <summary>
		/// Nombre de la propiedad de la instancia
		/// </summary>
		/// <returns></returns>
		public string PropertyName { get => _propertyName; set => _propertyName = value; }


		/// <summary>
		/// Obtiene el valor de la propiedad y el objeto especificado por la instancia
		/// </summary>
		public void GetValue()
		{
			// Obtengo y almaceno el valor
			var value = GetValueFrom(Object, PropertyName);

			// Si la función existe 
			OnValueGet.WrapInvoke(value);
		}

		/// <summary>
		/// Obtiene el valor de la propiedad y el objeto especificado por la instancia
		/// </summary>
		public void GetValue(string parseType)
		{
			// Obtengo y almaceno el valor
			var value = GetValueFrom(Object, PropertyName);

			// Si hay definido un valor para convertir el valor
			if (!string.IsNullOrEmpty(parseType))
			{
				// Obtengo el tipo según el parámetro
				System.Type typeParsed = System.Type.GetType(parseType);

				// Si el valor convertido no es nulo
				if (typeParsed != null)
				{
					// Cambio el tipo del objeto al tipo definido
					value = System.Convert.ChangeType(value, typeParsed);
				}
			}

			// Si la función existe 
			OnValueGet.WrapInvoke(value);
		}

		/// <summary>
		/// Obtiene el valor de la propiedad en el objeto especificado
		/// </summary>
		/// <param name="target">Objeto del cual se lee la propiedad</param>
		/// <param name="propertyName">Nombre de la propiedad a sacar</param>
		/// <returns>El valor de la propiedad definida en el objeto</returns>
		public static object GetValueFrom(Object target, string propertyName)
		{
			// Si el objeto no es nulo
			if (target != null)
			{
				// Obtengo el tipo del target
				var type = target.GetType();

				// Obtengo la propiedad del target dado el nombre
				var property = type.GetProperty(propertyName);

				// Si existe la propiedad
				if (property != null)
				{

					// Si no se puede leer la propiedad
					if (!property.CanRead)
					{
						Debug.LogError($"Can't read from property, probably missing a Get : {target} {propertyName}");
						return null;
					}
					// Obtengo su valor dado el objeto
					var value = property.GetValue(target);

					// Retorno el valor
					return value;
				}
				else
				{
					// Si no existe la propiedad
					Debug.LogError($"The property '{propertyName}' on {type} object, doesn't exist");
					return null;
				}
			}
			else
			{
				// No existe el objeto
				Debug.LogError($"The object to read the value from is null : {propertyName}");
				return null;
			}
		}
	}
}
