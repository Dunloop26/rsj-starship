﻿using System.Collections;
using System.Collections.Generic;
using Dunas.Events;
using UnityEngine;
using UnityEngine.Events;

namespace Dunas.Modules
{
	public class KeyListener : MonoBehaviour
	{
		public enum ListenMode { Press, Hold, Release }
		[SerializeField] private KeyCode _key;
		[SerializeField] ListenMode _keyListenMode;
		[Space]
		[SerializeField] private UnityEvent _response;

		public KeyCode Key { get => _key; set => _key = value; }
		public UnityEvent Response { get => _response; set => _response = value; }
		public ListenMode KeyListenMode { get => _keyListenMode; set => _keyListenMode = value; }

		public void Update()
		{
			switch (KeyListenMode)
			{
				case ListenMode.Press:
					// Ejecuto el evento al escuchar la tecla
					if (Input.GetKeyDown(Key))
						Response.WrapInvoke();
					break;
				case ListenMode.Hold:
					// Ejecuto el evento al escuchar la tecla
					if (Input.GetKey(Key))
						Response.WrapInvoke();
					break;
				case ListenMode.Release:
					// Ejecuto el evento al escuchar la tecla
					if (Input.GetKeyUp(Key))
						Response.WrapInvoke();
					break;
			}
		}
	}
}
