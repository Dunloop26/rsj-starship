﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Dunas.Modules
{
	/// <summary>
	/// Clase Helper para escribir en el Debug Log de Unity
	/// </summary>
	public class DebugLogger : MonoBehaviour
	{
		/// <summary>
		/// Mando un mensaje a través del Debug de Unity
		/// </summary>
		/// <param name="msg">Mensaje a escribir</param>
		public void Log(string msg)
		{
			// Si el mensaje no está vacío ni es nulo
			if (!string.IsNullOrEmpty(msg))
				//Logueo el mensaje
				Debug.Log(msg);
		}

		/// <summary>
		/// Logueo un objeto usando el debug de Unity
		/// </summary>
		/// <param name="unityObject">Objeto de Unity a loguear</param>
		public void Log(Object unityObject)
		{
			// Si el objeto no es nulo
			if (unityObject != null)
			{
				// Logueo el objeto
				Debug.Log(unityObject);
			}
		}
	}
}
