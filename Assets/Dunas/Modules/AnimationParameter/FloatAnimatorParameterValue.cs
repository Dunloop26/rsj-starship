﻿using System.Collections;
using System.Collections.Generic;
using Dunas.Events;
using UnityEngine;

namespace Dunas.Modules.AnimatorParameter
{
	public class FloatAnimatorParameterValue : MonoBehaviour
	{
		[Header("Component Reference")]
		[SerializeField] private Animator _animatorComponent;
		[Header("Settings")]
		[SerializeField] private string _parameterName;

		[Header("Events")]
		[SerializeField] private FloatEvent _onUpdateValue = new FloatEvent();

		public string ParameterName { get => _parameterName; set => _parameterName = value; }
		public Animator AnimatorComponent { get => _animatorComponent; set => _animatorComponent = value; }
		public FloatEvent OnUpdateValue { get => _onUpdateValue; }

		private void Awake()
		{
			if (!AnimatorComponent) AnimatorComponent = GetComponent<Animator>();
		}

		public void UpdateValue(float value)
		{
			if (!AnimatorComponent)
			{
				Debug.LogError($"There is no animator on this component, which is required : {this}", this);
				return;
			}
			
			if(string.IsNullOrEmpty(ParameterName)){
				Debug.LogError($"The parameter name is null or empty : {this}", this);
				return;
			}


			// Defino el valor del parámetro en el animator dado
			AnimatorComponent.SetFloat(ParameterName, value);

			// Comunico el evento
			OnUpdateValue.WrapInvoke(value);
		}
	}
}
