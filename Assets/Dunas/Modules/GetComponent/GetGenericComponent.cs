﻿using System.Collections;
using System.Collections.Generic;
using Dunas.Events;
using UnityEngine;
using UnityEngine.Events;

namespace Dunas.Modules.GetComponent
{
	public abstract class GetGenericComponent<T> : MonoBehaviour where T : Component
	{
		public enum GetComponentMode { Normal, Children, Parent }

		[Header("Value")]
		[SerializeField] private GameObject _value = null;
		[SerializeField] private GetComponentMode _getMode = GetComponentMode.Normal;
		[Header("Events")]
		[SerializeField] private UnityEvent _onSetValue = new UnityEvent();
		public UnityEvent<T> UEventResponse { get; set; }

		public bool IsInitialized => UEventResponse != null;

		public GameObject Value { get => _value; set { _value = value; _onSetValue.WrapInvoke(); } }
		public GetComponentMode GetMode { get => _getMode; set => _getMode = value; }
		public UnityEvent OnSetValue { get => _onSetValue; set => _onSetValue = value; }

		public abstract void Initialize();

		public void Get()
		{
			// Ejecuto la función con los parámetros ya definidos
			Get(Value);
		}

		public void Get(GameObject gObject)
		{
			// Obtengo la salida del componente
			T output = GetFromArgument(gObject);

			// Comunico el output
			UEventResponse.WrapInvoke(output);
		}
		public virtual T GetFromArgument(GameObject gObject)
		{
			// Genero el output
			T output = default(T);

			// Si no ha sido inicializado el evento
			if(!IsInitialized) Initialize();
 
			// Si el objeto no es nulo
			if (gObject != null)
			{
				// Defino el componente
				switch (GetMode)
				{
					case GetComponentMode.Normal:
						output = gObject.GetComponent<T>();
						break;
					case GetComponentMode.Children:
						output = gObject.GetComponentInChildren<T>();
						break;
					case GetComponentMode.Parent:
						output = gObject.GetComponentInParent<T>();
						break;
					default:
						break;
				}
			}
			else Debug.LogError($"The passed GameObject is null", this);

			// Retorno el output
			return output;
		}
	}
}
