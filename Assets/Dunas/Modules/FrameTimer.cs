﻿using System;
using System.Collections;
using System.Collections.Generic;
using Dunas.AssetEvents.Events;
using Dunas.Events;
using UnityEngine;
using UnityEngine.Events;

namespace Dunas.Modules
{

	/// <summary>
	/// Temporizador de Inspector para Unity
	/// </summary>
	public class FrameTimer : MonoBehaviour
	{
		[Header("Settings")]
		[SerializeField] private int _frames = 10;
		[SerializeField] private bool _fireOnAwake = false;
		[SerializeField] private bool _showDebug = false;
		[Header("Events")]
		[SerializeField] private UnityEvent _onTimerEnd = new UnityEvent();
		[SerializeField] private IntEvent _onFramesPassed = new IntEvent();
		[SerializeField] private FloatEvent _onFramesPassedNormalized = new FloatEvent();
		[SerializeField] private UnityEvent _onTimerStopped = new UnityEvent();


		private bool _stopped = false;

		/// <summary>
		/// ¿Está el temporizador en ejecución?
		/// </summary>
		/// <value>'true' si el temporizador está en ejecución, si no 'false'</value>
		public bool TimerRunning { get; private set; } = false;

		/// <summary>
		/// Frames definidos del temporizador
		/// </summary>
		/// <value>Valor 'int' de los frames</value>
		public int Frames
		{ get => _frames; set => _frames = value; }

		/// <summary>
		/// Evento de ejecución al finalizar el temporizador
		/// </summary>
		/// <value>UnityEvent para el evento de ejecución</value>
		public UnityEvent OnTimerEnd { get => _onTimerEnd; set => _onTimerEnd = value; }
		/// <summary>
		/// Evento de ejecución al interrumpir el temporizador
		/// </summary>
		/// <value>UnityEvent para el evento de ejecución</value>
		public UnityEvent OnTimerStopped { get => _onTimerStopped; set => _onTimerStopped = value; }
		public IntEvent OnSecondsPassed { get => _onFramesPassed; set => _onFramesPassed = value; }
		public FloatEvent OnSecondsPassedNormalized { get => _onFramesPassedNormalized; set => _onFramesPassedNormalized = value; }

		private void Awake()
		{
			// Si se puede disparar el temporizador
			if (_fireOnAwake)
			{
				// Disparo el temporizador en Awake
				FireTimer();
			}
		}

		/// <summary>
		/// Ejecuto el temporizador con el tiempo definido
		/// </summary>
		public void FireTimer()
		{

			if(_showDebug) Debug.Log($"Fire", this);

			// Ejecuto la rutina de temporizador
			StartCoroutine(
				FrameTimeEvent(_frames,             // Segundos a ejecutar el temporizador
				() => OnTimerEnd.Invoke(),      // Evento de ejecución al finalizar
				() => OnTimerStopped.Invoke(),  // Evento de ejecución al detener el temporizador
				(int framesElapsed) =>
				{
					// Ejecuto los eventos de segundos transcurridos
					_onFramesPassed.WrapInvoke(framesElapsed);
					_onFramesPassedNormalized.WrapInvoke(framesElapsed / (float)_frames);
				}
				)
			);

		}

		/// <summary>
		/// Detengo el temporizador
		/// </summary>
		public void StopTimer()
		{
			// Si el temporizador está corriendo
			if (TimerRunning)
			{
				// Detengo el temporizador
				_stopped = true;
			}
		}

		public IEnumerator FrameTimeEvent(int frames, Action onFinished = null, Action onCancelled = null, Action<int> onFramesElapsed = null)
		{

			// Si el temporizador se está ejecutando
			if (TimerRunning) yield break;  // No ejecuto la rutina

			if(_showDebug) Debug.Log($"Executing Timer : {gameObject.name}", this);

			// Defino como en ejecución la rutina
			TimerRunning = true;
			_stopped = false;

			// Ejecuto la espera
			// Defino el tiempo transcurrido inicial
			int elapsed = 0;

			// Ejecuto los eventos (si existen) de los segundos transcurridos
			if (onFramesElapsed != null) onFramesElapsed(0);

			do
			{
				// Espero un frame
				yield return 0;

				// Aumento la cuenta del tiempo transcurrido, por el tiempo delta
				elapsed++;

				// Ejecuto los eventos (si existen) de los segundos transcurridos
				if (onFramesElapsed != null) onFramesElapsed(elapsed);

			}
			while (elapsed < frames && !_stopped); // Mientras la ejecución no supere el tiempo límite y no se haya detenido por orden externa

			// Ejecuto los eventos (si existen) de los segundos transcurridos
			if (onFramesElapsed != null) onFramesElapsed(frames);


			// Si fue detenido el temporizador
			if (_stopped)
			{
				// Restablezco el estado actual de la orden de salida
				_stopped = false;

				// Ejecuto el evento de interrupción
				if (onCancelled != null) onCancelled();
			}
			else
			{
				// Si no fue detenido el temporizador

				// Ejecuto la rutina de finalización del temporizador
				if (onFinished != null) onFinished();
			}

			// Defino como finalizada la rutina
			TimerRunning = false;
		}
	}
}