﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Dunas.PopUp
{
  public class MessagePopUp : MonoBehaviour
  {
    [Header("Settings")]
    [SerializeField] private TextMeshProUGUI _titleText;
    [SerializeField] private TextMeshProUGUI _messageText;
    [Space]
    [SerializeField] private Button _closeButton;
    [SerializeField] private Button _acceptButton;

    [Header("Actions")]
    [SerializeField] private UnityEvent _onCloseActionPerformed = new UnityEvent();
    [SerializeField] private UnityEvent _onAcceptActionPerformed = new UnityEvent();

    public UnityEvent OnCloseActionPerformed => _onCloseActionPerformed;
    public UnityEvent OnAcceptActionPerformed => _onAcceptActionPerformed;

    public string Title
    {
      get => _titleText.text;
      set => _titleText.text = value;
    }
    public string Message
    {
      get => _messageText.text;
      set => _messageText.text = value;
    }

    private void OnEnable()
    {
      if (_titleText != null)
      {
        var titleGO = transform.Find("Title");
        if (titleGO != null)
        {
          _titleText = titleGO.GetComponent<TextMeshProUGUI>();
        }
      }

      if (_messageText != null)
      {
        var messageGO = transform.Find("Message");
        if (messageGO != null)
        {
          _messageText = messageGO.GetComponent<TextMeshProUGUI>();
        }
      }

      // Agrego funcionalidad del botón 'Close'
      if (_closeButton != null)
      {
        _closeButton.onClick.AddListener(Hide);
        _closeButton.onClick.AddListener(() =>
        {
          if (_onCloseActionPerformed != null)
          {
            _onCloseActionPerformed.Invoke();
          }
        });
      }

      // Agrego funcionalidad del botón 'Accept'
      if (_acceptButton != null)
      {
        _acceptButton.onClick.AddListener(Hide);
        _acceptButton.onClick.AddListener(() =>
        {
          if (_onAcceptActionPerformed != null)
          {
            _onAcceptActionPerformed.Invoke();
          }
        });
      }
    }

    public void Show(string title, string message)
    {
      // Defino los textos
      Title = title ?? string.Empty;
      Message = message ?? string.Empty;

      // Activo el PopUp
      gameObject.SetActive(true);
    }

    public void Show()
    {
      gameObject.SetActive(true);
    }

    public void Hide()
    {
      gameObject.SetActive(false);
    }
  }
}