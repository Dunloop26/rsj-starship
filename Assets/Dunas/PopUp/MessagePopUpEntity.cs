﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Dunas.Events;

namespace Dunas.PopUp
{
  public class MessagePopUpEntity : MonoBehaviour
  {

    [Header("Default Events")]
    [SerializeField] private UnityEvent _onAcceptActionPerformed = new UnityEvent();
    [SerializeField] private UnityEvent _onCloseActionPerformed = new UnityEvent();

    private bool initialized = false;

    private MessagePopUp _messagePopUp;

    /// <summary>
    /// Instancia de MessagePopUp
    /// </summary>
    /// <value></value>
    public MessagePopUp PopUp
    {
      get
      {
        // Si el campo es nulo, se define con el valor del manager
        if (_messagePopUp == null) _messagePopUp = PopUpManager.Instance.MessagePopUp;

        // Si no ha sido inicializado y hay un mensaje PopUp definido
        if (!initialized && _messagePopUp != null)
        {
          // Enlazo las acciones por defecto al PopUp
          _messagePopUp.OnAcceptActionPerformed.AddListener(() => _onAcceptActionPerformed.WrapInvoke());
          _messagePopUp.OnCloseActionPerformed.AddListener(() => _onCloseActionPerformed.WrapInvoke());
        }
        // Retorno la instacia del PopUp
        return _messagePopUp;
      }
    }

    /// <summary>
    /// Titulo que muestra el PopUp
    /// </summary>
    /// <value>Valor del texto a mostrar</value>
    public string Title
    {
      get => PopUp.Title;
      set => PopUp.Title = value;
    }

    /// <summary>
    /// Mensaje que muestra el PopUp
    /// </summary>
    /// <value>Valor del texto a mostrar</value>
    public string Message
    {
      get => PopUp.Message;
      set => PopUp.Message = value;
    }

    /// <summary>
    /// Muestro el PopUp
    /// </summary>
    public void Show() => PopUp.Show(Title, Message);
    /// <summary>
    /// Oculto el PopUp
    /// </summary>
    public void Hide() => PopUp.Hide();
  }
}
