﻿using System.Collections;
using System.Collections.Generic;
using Dunas.PopUp;
using UnityEngine;

public class LogClass
{
  public LogClass(string condition, string stack, LogType type)
  {
    Condition = condition;
    Stack = stack;
    Type = type;
  }

  private string _condition;
  private string _stack;
  private LogType _type;

  public string Condition { get => _condition; set => _condition = value; }
  public string Stack { get => _stack; set => _stack = value; }
  public LogType Type { get => _type; set => _type = value; }
}

public class ErrorPopUpDisplay : MonoBehaviour
{

  private MessagePopUp _messagePopUp;
  private Queue<LogClass> _logQueue;

  private List<LogClass> _errorList;

  private void Awake()
  {
    DontDestroyOnLoad(gameObject);

    _logQueue = new Queue<LogClass>();

    var popUpManager = PopUpManager.Instance;

    _messagePopUp = popUpManager.MessagePopUp;

    Application.logMessageReceived += (condition, stack, type) =>
    {
      var log = new LogClass(condition, stack, type);

      _logQueue.Enqueue(log);
      HandleNextLog();

      // Lo devuelvo a la queue, ya que HandleNextLog lo elimina
      _logQueue.Enqueue(log);
    };

    Application.logMessageReceived += LogErrors;

    _messagePopUp.Show("Test Message", "Working");
    _messagePopUp.OnAcceptActionPerformed.AddListener(HandleNextLog);
  }

  private void LogErrors(string condition, string stack, LogType type)
  {

    if (type.Equals(LogType.Error) || type.Equals(LogType.Warning) || type.Equals(LogType.Exception))
    {
      var log = new LogClass(condition, stack, type);
      _errorList.Add(log);
    }
  }

  private void HandleNextLog()
  {

    if (_logQueue.Count > 0)
    {

      var nextLog = _logQueue.Dequeue();

      if (nextLog != null)
      {
        _messagePopUp.Show(
          $"{System.Enum.GetNames(typeof(LogType))[(int)nextLog.Type]} Message",
          $"{nextLog.Condition}"
        );
      }
    }
    else
    {
      _messagePopUp.Hide();
    }
  }
}