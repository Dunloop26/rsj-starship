﻿using System.Collections;
using System.Collections.Generic;
using Dunas.AssetData;
using UnityEngine;
using UnityEngine.UI;

namespace Dunas.PopUp
{

  public class PopUpManager : MonoBehaviour
  {

    private static PopUpManager _instance;
    public static PopUpManager Instance
    {
      get
      {
        // Si la instancia no existe
        if (_instance == null)
        {
          Initialize();
        }
        return _instance;
      }
    }

    private MessagePopUp _messagePopUp;
    public MessagePopUp MessagePopUp => _messagePopUp;

    private Canvas _canvas;
    private GraphicRaycaster _graphicRaycaster;

    private static void Initialize()
    {

      // Si la instancia no existe
      if (_instance == null)
      {


        // Creo un nuevo objeto
        var GO = new GameObject("PopUpManager");

        // Marco el objeto para no destruirse
        DontDestroyOnLoad(GO);

        // Le anexo la instancia
        _instance = GO.AddComponent<PopUpManager>();

        // Agrego el canvas scaler
        var scaler = GO.AddComponent<CanvasScaler>();

        // Configuro los parámetros del scaler
        scaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
        scaler.screenMatchMode = CanvasScaler.ScreenMatchMode.MatchWidthOrHeight;
        scaler.referenceResolution = new Vector2(800f, 400f);
        scaler.matchWidthOrHeight = 0f;
        scaler.referencePixelsPerUnit = 100f;

        // Defino el canvas del objeto
        _instance._canvas = GO.GetComponent<Canvas>();
        _instance._canvas.renderMode = RenderMode.ScreenSpaceOverlay;
        _instance._canvas.sortingOrder = 50;

        // Creo un GraphicRaycaster para la interacción de botones y demás
        _instance._graphicRaycaster = GO.AddComponent<GraphicRaycaster>();

        var msgPopUpPrefab = ProjectAssetLoader.GetAssetFromList<GameObject>(asset =>
            asset.Id.Equals("MessagePopUp")
        );

        _instance._messagePopUp = Instantiate(msgPopUpPrefab, _instance._canvas.transform).GetComponent<MessagePopUp>();
      }

    }

  }
}