﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Dunas.Coroutine
{
  public class CoroutineRunner : MonoBehaviour
  {
    private static CoroutineRunner _instance;
    private static CoroutineRunner Instance
    {
      get
      {
        if (_instance == null) _instance = CreateInstance<CoroutineRunner>();
        return _instance;
      }
    }

    private static T CreateInstance<T>(string name = null) where T : MonoBehaviour
    {
      if (string.IsNullOrEmpty(name)) name = typeof(T).Name;
      return new GameObject(name).AddComponent<T>();
    }

    public class RunnerObject
    {
      public RunnerObject(IEnumerator coroutine, CoroutineRunner coroutineRunner)
      {
        RawCoroutine = coroutine;
        CoroutineRunner = coroutineRunner;
      }
      public event EventHandler Started;
      public event EventHandler Ended;
      public CoroutineRunner CoroutineRunner { get; }
      public IEnumerator Coroutine
      {
        get
        {
          return WrappedCoroutine();
        }
      }
      public IEnumerator RawCoroutine { get; }
      public bool Running { get; private set; }
      public UnityEngine.Coroutine UCoroutine { get; private set; }
      private IEnumerator WrappedCoroutine()
      {
        Running = true;
        Started?.Invoke(this, new EventArgs());

        yield return RawCoroutine;

        Running = false;
        Ended?.Invoke(this, new EventArgs());
      }
      public void Stop()
      {
        Running = false;
        CoroutineRunner.Stop(this);
        UCoroutine = null;
      }

      public void Run()
      {
        Running = true;
        UCoroutine = CoroutineRunner.Run(this);
      }
    }

    private void Stop(RunnerObject runnerObject)
    {
      if (runnerObject == null || runnerObject.UCoroutine == null) return;
      if (_runners.Contains(runnerObject)) _runners.Remove(runnerObject);
      StopCoroutine(runnerObject.UCoroutine);
    }

    private UnityEngine.Coroutine Run(RunnerObject runnerObject)
    {
      if (runnerObject == null) return null;
      if (_runners.Contains(runnerObject)) return null;
      _runners.Add(runnerObject);
      return StartCoroutine(runnerObject.Coroutine);
    }

    private List<RunnerObject> _runners = new List<RunnerObject>();
    public static RunnerObject BuildRunnerFromInstance(IEnumerator coroutine)
    {
      return Instance.BuildRunner(coroutine);
    }
    public RunnerObject BuildRunner(IEnumerator coroutine)
    {
      RunnerObject output = new RunnerObject(coroutine, this);
      return output;
    }
  }
}