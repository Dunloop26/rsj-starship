﻿using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using ResponseTypeEvent = Dunas.AssetEvents.Events.UnityObjectAssetEvent.UnityObjectEvent;
public class PropertyWatcher : MonoBehaviour
{
	[Header("Settings")]
	[SerializeField] private UnityEngine.Object _objectValue;
	[SerializeField] private string _parseType;
	[SerializeField] private string _propertyName;

	[Header("Events")]
	[SerializeField] private ResponseTypeEvent _onNotNullCall;
	[SerializeField] private ResponseTypeEvent _onNewDefineCall;
	[Space]
	[SerializeField] private ResponseTypeEvent _onSetNewValue;
	[SerializeField] private ResponseTypeEvent _onGetCurrentValue;

	public UnityEngine.Object ObjectValue { get => _objectValue; set => _objectValue = value; }
	public string PropertyName { get => _propertyName; set => _propertyName = value; }
	private string _storedPropertyName { get; set; }
	private PropertyInfo _property { get; set; }
	public string ParseType { get => _parseType; set => _parseType = value; }

	private void Update()
	{
		// Proceso la definición de la propiedad	
		PropertyDefinitionProcess();

		// Si la propiedad ha sido definida
		if (_property != null)
		{
			// Configurar Warcher
			// TODO: Completar código
		}

	}

	private void PropertyDefinitionProcess()
	{
		// Si la propiedad es nula o el nombre de la propiedad ha cambiado
		if (_property == null || !_storedPropertyName.Equals(PropertyName))
		{
			// Defino la propiedad

			// Obtengo el tipo
			var typeToParse = Type.GetType(_parseType);

			// Si el tipo no es nulo
			if (typeToParse != null)
			{

				// Obtengo la propiedad si se ha definido un nombre
				if (!string.IsNullOrEmpty(PropertyName))
				{
					// Busco la propiedad en el tipo
					_property = typeToParse.GetProperty(PropertyName);

					// Defino el nombre de la propiedad anterior
					_storedPropertyName = PropertyName;
				}
				else
				{
					// El nombre de la propiedad es nulo o está vacío
					Debug.LogError($"The property name {PropertyName} is null or empty", this);
					return;
				}
			}
			else
			{
				// El tipo es nulo
				Debug.LogError($"The type of {_parseType} couldn't be found", this);
				return;
			}
		}
	}

}
