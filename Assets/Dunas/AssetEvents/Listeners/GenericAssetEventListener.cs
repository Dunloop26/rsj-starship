﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
namespace Dunas.AssetEvents
{

	[System.Serializable]
	public abstract class GenericAssetEventListener<T> : MonoBehaviour
	{

		public abstract void OnEnable();
		public abstract void OnDisable();

		public abstract void OnEventRaised(T arg0);

	}

}
