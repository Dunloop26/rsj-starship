﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Dunas.Events;
using Dunas.AssetEvents.Events;

using Object = System.Object;

namespace Dunas.AssetEvents.Listeners
{
  [System.Serializable]
  public class SystemObjectAssetEventListener : GenericAssetEventListener<Object>
  {
    [Header("Listener Settings")]
    [SerializeField] private SystemObjectAssetEvent _assetEvent;
    [SerializeField] private SystemObjectAssetEvent.SystemObjectEvent _response;

    public override void OnEnable() => _assetEvent.AddListener(this);
    public override void OnDisable() => _assetEvent.RemoveListener(this);

    public override void OnEventRaised(Object arg0)
    {
      if (_response != null) _response.WrapInvoke(arg0);
    }
  }
}
