﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Dunas.Events;
using Dunas.AssetEvents.Events;

namespace Dunas.AssetEvents.Listeners
{
  [System.Serializable]
  public class StringAssetEventListener : GenericAssetEventListener<string>
  {
    [Header("Listener Settings")]
    [SerializeField] private StringAssetEvent _assetEvent;
    [SerializeField] private StringAssetEvent.StringEvent _response;

    public override void OnEnable() => _assetEvent.AddListener(this);
    public override void OnDisable() => _assetEvent.RemoveListener(this);

    public override void OnEventRaised(string arg0)
    {
      if (_response != null) _response.WrapInvoke(arg0);
    }
  }
}
