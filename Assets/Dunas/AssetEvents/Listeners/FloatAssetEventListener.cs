﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Dunas.Events;
using Dunas.AssetEvents.Events;

namespace Dunas.AssetEvents.Listeners
{
	[System.Serializable]
	public class FloatAssetEventListener : GenericAssetEventListener<float>
	{
		[Header("Listener Settings")]
		[SerializeField] private FloatAssetEvent _assetEvent;
		[SerializeField] private FloatAssetEvent.FloatEvent _response;

		public override void OnEnable() => _assetEvent.AddListener(this);
		public override void OnDisable() => _assetEvent.RemoveListener(this);

		public override void OnEventRaised(float arg0)
		{
			if (_response != null) _response.WrapInvoke(arg0);
		}
	}
}
