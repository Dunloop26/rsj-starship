﻿using System.Collections;
using System.Collections.Generic;
using Dunas.Events;
using UnityEngine;
using Dunas.AssetEvents.Events;

namespace Dunas.AssetEvents.Listeners
{
  public class CameraAssetEventListener : GenericAssetEventListener<Camera>
  {
    [Header("Listener Settings")]
    [SerializeField] private CameraAssetEvent _assetEvent;
    [SerializeField] private CameraAssetEvent.CameraEvent _response;

    public override void OnEnable() => _assetEvent.AddListener(this);
    public override void OnDisable() => _assetEvent.RemoveListener(this);

    public override void OnEventRaised(Camera arg0)
    {
      if (_response != null) _response.WrapInvoke(arg0);
    }
  }
}
