﻿using System.Collections;
using System.Collections.Generic;
using Dunas.Events;
using UnityEngine;
using Dunas.AssetEvents.Events;

namespace Dunas.AssetEvents.Listeners
{
  public class GameObjectAssetEventListener : GenericAssetEventListener<GameObject>
  {
    [Header("Listener Settings")]
    [SerializeField] private GameObjectAssetEvent _assetEvent;
    [SerializeField] private GameObjectAssetEvent.GameObjectEvent _response;

    public override void OnEnable() => _assetEvent.AddListener(this);
    public override void OnDisable() => _assetEvent.RemoveListener(this);

    public override void OnEventRaised(GameObject arg0)
    {
      if (_response != null) _response.WrapInvoke(arg0);
    }
  }
}
