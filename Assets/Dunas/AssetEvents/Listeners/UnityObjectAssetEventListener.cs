﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Dunas.Events;
using Dunas.AssetEvents.Events;

using Object = UnityEngine.Object;

namespace Dunas.AssetEvents.Listeners
{
  [System.Serializable]
  public class UnityObjectAssetEventListener : GenericAssetEventListener<Object>
  {
    [Header("Listener Settings")]
    [SerializeField] private UnityObjectAssetEvent _assetEvent;
    [SerializeField] private UnityObjectAssetEvent.UnityObjectEvent _response;

    public override void OnEnable() => _assetEvent.AddListener(this);
    public override void OnDisable() => _assetEvent.RemoveListener(this);

    public override void OnEventRaised(Object arg0)
    {
      if (_response != null) _response.WrapInvoke(arg0);
    }
  }
}
