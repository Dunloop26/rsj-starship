﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Dunas.AssetEvents.Events;

namespace Dunas.AssetEvents.Listeners
{
    [System.Serializable]
    public class AssetEventListener : MonoBehaviour
    {
        [Header("Listener Settings")]
        [SerializeField] private AssetEvent _gameEvent;
        [SerializeField] private UnityEvent _response;

        private void OnEnable() => _gameEvent.AddListener(this);
        private void OnDisable() => _gameEvent.RemoveListener(this);

        [ContextMenu("Call event raised")]
        public void OnEventRaised()
        {
            if (_response != null)
                _response.Invoke();
        }

    }
}