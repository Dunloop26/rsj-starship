﻿using System.Collections;
using System.Collections.Generic;
using Dunas.Events;
using UnityEngine;
using Dunas.AssetEvents.Events;

namespace Dunas.AssetEvents.Listeners
{
  public class TransformAssetEventListener : GenericAssetEventListener<Transform>
  {
    [Header("Listener Settings")]
    [SerializeField] private TransformAssetEvent _assetEvent;
    [SerializeField] private TransformAssetEvent.TransformEvent _response;

    public override void OnEnable() => _assetEvent.AddListener(this);
    public override void OnDisable() => _assetEvent.RemoveListener(this);

    public override void OnEventRaised(Transform arg0)
    {
      if (_response != null) _response.WrapInvoke(arg0);
    }
  }
}
