﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Dunas.AssetEvents.Events
{
  [System.Serializable]
  [CreateAssetMenu(menuName = "Dunas/AssetEvents/New String Event", fileName = "NewStringEvent")]
  public class StringAssetEvent : GenericAssetEvent<string>
  {
    [System.Serializable] public class StringEvent : UnityEvent<string> { }
  }
}