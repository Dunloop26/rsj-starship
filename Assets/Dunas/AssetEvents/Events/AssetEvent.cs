﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Dunas.AssetEvents.Listeners;
using Dunas.Events;

namespace Dunas.AssetEvents.Events
{
	[System.Serializable]
	[CreateAssetMenu(menuName = "Dunas/AssetEvents/New Asset Event", fileName = "NewAssetEvent", order = 0)]
	public class AssetEvent : ScriptableObject
	{
		[Header("Description")]
		[Multiline()]
		[SerializeField] private string _devDescription;

		[Header("Listeners")]
		[SerializeField] private List<AssetEventListener> _listeners = new List<AssetEventListener>();
		[Header("Debug")]
		[SerializeField] private bool _showDebug;
		public bool ShowDebug { get => _showDebug; set => _showDebug = value; }

		public void RaiseTrigger()
		{
			RaiseTriggerEvent();

			// Recorro cada listener
			for (int i = _listeners.Count - 1; i >= 0; i--)
			{
				// Llamo el evento en cada listener
				_listeners[i].OnEventRaised();
			}

			if (ShowDebug) Debug.Log($"Delivered {_listeners.Count} asset listener responses at {this}", this);
		}

		private void RaiseTriggerEvent()
		{
			if (ShowDebug) Debug.Log($"Trigger Raised with \"{name}\" ID at {this}", this);
			// Llamo el event manager
			EventManager.TriggerEvent(name, new System.EventArgs(), this);
		}

		public void AddListener(AssetEventListener assetEventListener)
		{
			// Si no está en la lista, lo registro
			if (!_listeners.Contains(assetEventListener)) _listeners.Add(assetEventListener);
			else Debug.LogWarning($"GameEventListener '{assetEventListener.name}' already present in the list: {name}", this);

			if (ShowDebug) Debug.Log($"Added {assetEventListener} as listener to {this}", this);
		}

		public void RemoveListener(AssetEventListener assetEventListener)
		{
			// Si se encuentra en la lista, lo elimino
			if (_listeners.Contains(assetEventListener)) _listeners.Remove(assetEventListener);
			else Debug.LogWarning($"GameEventListener '{assetEventListener.name}' not present on the list : {name}", this);

			if (ShowDebug) Debug.Log($"Removed {assetEventListener} as listener to {this}", this);
		}
	}
}