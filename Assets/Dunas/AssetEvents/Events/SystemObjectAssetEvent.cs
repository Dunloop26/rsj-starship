﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

using Object = System.Object;

namespace Dunas.AssetEvents.Events
{
  [System.Serializable]
  [CreateAssetMenu(menuName = "Dunas/AssetEvents/New SystemObject Event", fileName = "NewSystemObjectEvent")]
  public class SystemObjectAssetEvent : GenericAssetEvent<Object>
  {
    [System.Serializable] public class SystemObjectEvent : UnityEvent<Object> { }
  }
}