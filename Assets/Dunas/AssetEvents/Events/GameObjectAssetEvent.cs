﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Dunas.AssetEvents.Events
{

  [System.Serializable]
  [CreateAssetMenu(menuName = "Dunas/AssetEvents/New GameObject Event", fileName = "NewGameObjectEvent")]
  public class GameObjectAssetEvent : GenericAssetEvent<GameObject>
  {
    [System.Serializable] public class GameObjectEvent : UnityEvent<GameObject> { }
  }
}
