﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Dunas.AssetEvents.Events
{

	[System.Serializable]
	[CreateAssetMenu(menuName = "Dunas/AssetEvents/New Bool Event", fileName = "NewBoolEvent")]
	public class BoolAssetEvent : GenericAssetEvent<bool>
	{
		[System.Serializable] public class BoolEvent : UnityEvent<bool> { }
	}

}