﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Dunas.AssetEvents.Events
{

	[System.Serializable]
	[CreateAssetMenu(menuName = "Dunas/AssetEvents/New Float Event", fileName = "NewFloatEvent")]
	public class FloatAssetEvent : GenericAssetEvent<float>
	{
		[System.Serializable] public class FloatEvent : UnityEvent<float> { }
	}

}