﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

using Object = UnityEngine.Object;

namespace Dunas.AssetEvents.Events
{
  [System.Serializable]
  [CreateAssetMenu(menuName = "Dunas/AssetEvents/New UnityObject Event", fileName = "NewUnityObjectEvent")]
  public class UnityObjectAssetEvent : GenericAssetEvent<Object>
  {
    [System.Serializable] public class UnityObjectEvent : UnityEvent<Object> { }
  }
}