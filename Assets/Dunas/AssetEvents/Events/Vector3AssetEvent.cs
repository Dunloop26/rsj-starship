﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Dunas.AssetEvents.Events
{

	[System.Serializable]
	[CreateAssetMenu(menuName = "Dunas/AssetEvents/New Vector3 Event", fileName = "NewVector3Event")]
	public class Vector3AssetEvent : GenericAssetEvent<Vector3>
	{
		[System.Serializable] public class Vector3Event : UnityEvent<Vector3> { }
	}
}