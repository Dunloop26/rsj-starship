﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Dunas.AssetEvents.Events
{

	[System.Serializable]
	[CreateAssetMenu(menuName = "Dunas/AssetEvents/New Int Event", fileName = "NewIntEvent")]
	public class IntAssetEvent : GenericAssetEvent<int>
	{
		[System.Serializable] public class IntEvent : UnityEvent<int> { }
	}
}