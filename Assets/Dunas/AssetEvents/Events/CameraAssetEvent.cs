﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Dunas.AssetEvents.Events
{

  [System.Serializable]
  [CreateAssetMenu(menuName = "Dunas/AssetEvents/New Camera Event", fileName = "NewCameraEvent")]
  public class CameraAssetEvent : GenericAssetEvent<Camera>
  {
    [System.Serializable] public class CameraEvent : UnityEvent<Camera> { }
  }
}
