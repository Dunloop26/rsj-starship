﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Dunas.AssetEvents.Events
{

  [System.Serializable]
  [CreateAssetMenu(menuName = "Dunas/AssetEvents/New Transform Event", fileName = "NewTransformEvent")]
  public class TransformAssetEvent : GenericAssetEvent<Transform>
  {
    [System.Serializable] public class TransformEvent : UnityEvent<Transform> { }
  }
}
