﻿using System.Collections;
using System.Collections.Generic;
using Dunas.Events;
using UnityEngine;
using UnityEngine.Events;
using System.Linq;

namespace Dunas.AssetEvents.Events
{
  [System.Serializable]
  public abstract class GenericAssetEvent<T> : ScriptableObject
  {

    [Header("Description")]
    [Multiline(3)]
    [SerializeField] private string _devDescription;

    [Header("Listener GameObjects")]
    [SerializeField] private GameObject[] _gameObjectListeners = new GameObject[0];
    
    [Header("Debug")]
    [SerializeField] private bool _showDebug;
    public bool ShowDebug { get => _showDebug; set => _showDebug = value; }


    private List<GenericAssetEventListener<T>> _listeners = new List<GenericAssetEventListener<T>>();
    public List<GenericAssetEventListener<T>> Listeners => _listeners;

    protected UnityEvent<T> _event;

    public virtual void RaiseTrigger(T arg0)
    {
      if (arg0 == null) return;

      // Ejecuto el trigger
      RaiseTriggerEvent(arg0);

      // Recorro cada listener
      for (int i = Listeners.Count - 1; i >= 0; i--)
      {
        // Llamo el evento en cada listener
        Listeners[i].OnEventRaised(arg0);
      }

      if (ShowDebug) Debug.Log($"Delivered {_listeners.Count} listener responses at", this);
    }

    private void RaiseTriggerEvent(T arg0)
    {
      if (ShowDebug) Debug.Log($"Trigger Raised with \"{name}\" ID at", this);
      // Llamo el event manager
      EventManager.TriggerEvent(name, new GenericEventArgs<T>(arg0), this);
    }

    public void AddListener(GenericAssetEventListener<T> assetEventListener)
    {
      // Si no está en la lista, lo registro
      if (!_listeners.Contains(assetEventListener)) _listeners.Add(assetEventListener);
      else Debug.LogWarning($"GameEventListener '{assetEventListener.name}' already present in the list: {name}");

      // Actualizo la lista de listeners para DEV Display
      _gameObjectListeners = (from i in _listeners
                              select i.gameObject).ToArray();

      if (ShowDebug) Debug.Log($"Added {assetEventListener} as listener to", this);
    }

    public void RemoveListener(GenericAssetEventListener<T> assetEventListener)
    {
      // Si se encuentra en la lista, lo elimino
      if (_listeners.Contains(assetEventListener)) _listeners.Remove(assetEventListener);
      else Debug.LogWarning($"GameEventListener '{assetEventListener.name}' not present on the list : {name}");

      // Actualizo la lista de listeners para DEV Display
      _gameObjectListeners = (from i in _listeners
                              select i.gameObject).ToArray();

      if (ShowDebug) Debug.Log($"Removed {assetEventListener} as listener to", this);
    }
  }

}
