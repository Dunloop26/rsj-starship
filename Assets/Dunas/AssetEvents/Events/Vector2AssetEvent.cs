﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Dunas.AssetEvents.Events
{

	[System.Serializable]
	[CreateAssetMenu(menuName = "Dunas/AssetEvents/New Vector2 Event", fileName = "NewVector2Event")]
	public class Vector2AssetEvent : GenericAssetEvent<Vector2>
	{
		[System.Serializable] public class Vector2Event : UnityEvent<Vector2> { }
	}
}