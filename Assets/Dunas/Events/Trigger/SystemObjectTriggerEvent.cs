﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Dunas.Events;

namespace Dunas.Events
{
	public class SystemObjectTriggerEvent : GenericTriggerEvent<System.Object>
	{
		[Header("Events")]
		[SerializeField] private SystemObjectEvent _onValueSet;

		private void Start() => OnValueSet = _onValueSet;
	}
}
