﻿using System;
using System.Collections;
using System.Collections.Generic;
using Dunas.Events;
using UnityEngine;
using UnityEngine.Events;
using static Dunas.Events.EventManager;

namespace Dunas.Events.Trigger
{
	public class TriggerEvent : MonoBehaviour
	{
		[SerializeField] private string _triggerName;
		[Header("Or")]
		[Tooltip("Used in case of \"TriggerName\" evaluating to null")]
		[SerializeField] private ScriptableObject _scriptableObject;
		[Header("Events")]
		[SerializeField] private UnityEvent _onTriggerFired;

		public string TriggerName { get => _triggerName; set => _triggerName = value; }
		public string IDName
		{
			get
			{
				// Toma por ID el nombre definido en el triggerName, si este es nulo, se usa el nombre del scriptable
				return !string.IsNullOrEmpty(TriggerName) ? TriggerName : ScriptableObjectValue != null ? ScriptableObjectValue.name : string.Empty;
			}
		}
		public UnityEvent OnTriggerFired { get => _onTriggerFired; set => _onTriggerFired = value; }
		public ScriptableObject ScriptableObjectValue { get => _scriptableObject; set => _scriptableObject = value; }

		public void Trigger()
		{
			EventManager.TriggerEvent(IDName, null,
		 	this);
		}
	}
}
