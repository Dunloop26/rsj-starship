﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Dunas.Events
{
	public abstract class GenericTriggerEvent<T> : MonoBehaviour
	{
		[Header("Trigger Settings")]
		[SerializeField] private bool _showDebug;
		[SerializeField] private string triggerName;

		[Header("Or")]
		[Tooltip("Used in case of \"TriggerName\" evaluating to null")]
		[SerializeField] private ScriptableObject _scriptableObject;
		[Space]
		[Header("Value")]
		[SerializeField] private T _value;
		[SerializeField] private UnityEvent _onTriggerFired;
		public T Value { get => _value; set { _value = value; OnValueSet.WrapInvoke(_value); } }
		public bool IsInitialized => OnTriggerFired != null && !string.IsNullOrEmpty(IDName);
		public string TriggerName { get => triggerName; set => triggerName = value; }
		public string IDName
		{
			get
			{
				// Toma por ID el nombre definido en el triggerName, si este es nulo, se usa el nombre del scriptable
				return !string.IsNullOrEmpty(TriggerName) ? TriggerName : ScriptableObjectValue != null ? ScriptableObjectValue.name : string.Empty;
			}
		}
		public UnityEvent OnTriggerFired { get => _onTriggerFired; }
		public UnityEvent<T> OnValueSet { get; set; }
		public bool ShowDebug { get => _showDebug; set => _showDebug = value; }
		public ScriptableObject ScriptableObjectValue { get => _scriptableObject; set => _scriptableObject = value; }

		/// <summary>
		/// Comunica los argumentos con la construcción de argumentos interna
		/// </summary>
		public void Trigger()
		{
			ComunicateArguments(BuildArguments());
		}

		/// <summary>
		/// Llamada interna para construir los argumentos necesarios para que el trigger generico funcione correctamente
		/// </summary>
		/// <returns>Event args con los argumentos del evento</returns>
		public virtual EventArgs BuildArguments()
		{
			// Genero argumentos genericos y los retorno
			var output = new GenericEventArgs<T>(Value);
			return output;
		}

		/// <summary>
		/// Ejecuta el evento con parámetros por defecto, debe tener una llamada a ComunicateArguments de tipo T
		/// </summary>
		/// <param name="arg">Argumento sobre el cual construir un EventArgs</param>
		public virtual void TriggerCustomArgument(T arg)
		{
			// Genero el objeto de argumentos
			var output = new GenericEventArgs<T>(arg);
			// Comunico los argumentos construidos
			ComunicateArguments(output);
		}

		/// <summary>
		/// Comunica los argumentos al evento definido
		/// </summary>
		/// <param name="args">Argumentos del evento</param>
		public void ComunicateArguments(System.EventArgs args)
		{
			// Si habilito el debug
			if (ShowDebug) Debug.Log($"Trigger event \"{IDName}\" send at {this} with {args}");
			// Si no se ha inicializado retorna
			if (!IsInitialized) { Debug.LogError($"The instance is not initialized", this); ; return; }
			// Ejecuto el evento interno
			EventManager.TriggerEvent(IDName, args,
		 	this);
			// Ejecuto el evento
			OnTriggerFired.WrapInvoke();
		}
	}
}
