﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Dunas.Events;

using Float = System.Single;

namespace Dunas.Events
{
	public class FloatTriggerEvent : GenericTriggerEvent<Float>
	{
		[Header("Events")]
		[SerializeField] private FloatEvent _onValueSet;

		private void Start() => OnValueSet = _onValueSet;
	}
}
