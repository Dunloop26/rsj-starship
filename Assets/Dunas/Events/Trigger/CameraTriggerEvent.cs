﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Dunas.Events;

namespace Dunas.Events
{
	public class CameraTriggerEvent : GenericTriggerEvent<Camera>
	{
		[Header("Events")]
		[SerializeField] private CameraEvent _onValueSet;

		private void Start() => OnValueSet = _onValueSet;
	}
}
