﻿using System;
using System.Collections;
using System.Collections.Generic;
using Dunas.Events;
using UnityEngine;
using UnityEngine.Events;
using static Dunas.Events.EventManager;

using Float = System.Single;

namespace Dunas.Events
{
	public class FloatTriggerEventListener : GenericTriggerEventListener<float>
	{

		[SerializeField] private FloatEvent _response;


		private new void OnEnable() => SubscribeResponse();
		private void OnDisable() => UnsubscribeResponse();
		public override void Initialize() => UEventResponse = _response;
	}
}
