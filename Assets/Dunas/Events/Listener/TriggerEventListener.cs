﻿using System;
using System.Collections;
using System.Collections.Generic;
using Dunas.Events;
using UnityEngine;
using UnityEngine.Events;
using static Dunas.Events.EventManager;

namespace Dunas.Events
{
	public class TriggerEventListener : AbstractTriggerEventListener
	{
		[SerializeField] private UnityEvent _response;

		private new void OnEnable()
		{
			EventResponse += Response;
			base.OnEnable();
		}

		private new void OnDestroy()
		{
			EventResponse -= Response;
			base.OnDestroy();
		}

		public void Response(object sender, EventArgs args)
		{
			// Ejecuto el evento
			_response.WrapInvoke();
		}
	}
}
