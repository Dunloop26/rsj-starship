﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

namespace Dunas.Events
{
	public abstract class AbstractTriggerEventListener : MonoBehaviour
	{

		[Header("Listener Settings")]
		[SerializeField] private bool _showDebug = false;
		[SerializeField] private string _triggerName;
		[Header("Or")]
		[Tooltip("Used in case of \"TriggerName\" evaluating to null")]
		[SerializeField] private ScriptableObject _scriptableObject;

		public event Action<object, EventArgs> EventResponse;
	

		public string TriggerName { get => _triggerName; set => _triggerName = value; }
		public string IDName
		{
			get
			{
				// Toma por ID el nombre definido en el triggerName, si este es nulo, se usa el nombre del scriptable
				return !string.IsNullOrEmpty(TriggerName) ? TriggerName : ScriptableObjectValue != null ? ScriptableObjectValue.name : string.Empty;
			}
		}
		public bool ShowDebug { get => _showDebug; set => _showDebug = value; }
		public ScriptableObject ScriptableObjectValue { get => _scriptableObject; set => _scriptableObject = value; }

		private bool isActive = false;

		protected void OnEnable()
		{
			if (ShowDebug) Debug.Log($"Event Start Listening with \"{IDName}\" ID at {this}", this);
			EventManager.StartListening(IDName, EventHandler);
		}
		protected void OnDestroy()
		{
			if (ShowDebug) Debug.Log($"Event Stop Listening with \"{IDName}\" ID at {this}", this);
			EventManager.StopListening(IDName, EventHandler);
		}

		private void EventHandler(object sender, System.EventArgs args)
		{
			if (ShowDebug) Debug.Log($"Trigger event \"{IDName}\" received at {this} from {sender} with {args}", this);
			if (EventResponse != null) EventResponse(sender, args);
		}
	}
}
