﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

namespace Dunas.Events
{
	public abstract class GenericTriggerEventListener<T> : AbstractTriggerEventListener
	{

		public UnityEvent<T> UEventResponse { get; set; }

		/// <summary>
		/// Comprueba que los valores estén inicializados
		/// </summary>
		public bool IsInitialized => UEventResponse != null;

		protected void SubscribeResponse()
		{
			EventResponse += Response;
			Initialize();
			base.OnEnable();
		}

		protected void UnsubscribeResponse()
		{
			EventResponse -= Response;
			Initialize();
			base.OnDestroy();
		}

		/// <summary>
		/// Verifica si el tipo contiene la interfaz generica
		/// </summary>
		/// <param name="argsType">Tipo a evaluar</param>
		/// <param name="interfaceType">Interfaz a evaluar</param>
		/// <returns></returns>
		public bool ImplementsGenericInterface(Type argsType, Type interfaceType)
		{
			return argsType.GetInterfaces().Any(i => i.IsGenericType && i.GetGenericTypeDefinition().Equals(interfaceType));
		}

		/// <summary>
		/// Defino un método abstracto para inicializar la clase
		/// </summary>
		public abstract void Initialize();

		public void SendArgs(System.EventArgs args, UnityEvent<T> response)
		{
			// El tipo de los argumentos puede ser IValue
			var argsType = args.GetType();
			Type interfaceType = typeof(IValue<>);

			if (ImplementsGenericInterface(argsType, interfaceType))
			{
				var castedArgs = args as IValue<T>;

				// Si el valor no es nulo
				if (castedArgs.Value != null)
				{
					// Lanzo el evento
					response.WrapInvoke<T>(castedArgs.Value);
				}
			}
		}

		public virtual void Response(object sender, EventArgs args)
		{
			// Si los argumenos existen
			if (args != null)
			{
				// Envío los argumentos
				SendArgs(args, UEventResponse);
			}
		}
	}
}
