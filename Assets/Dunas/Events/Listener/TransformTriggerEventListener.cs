﻿using System;
using System.Collections;
using System.Collections.Generic;
using Dunas.Events;
using UnityEngine;
using UnityEngine.Events;
using static Dunas.Events.EventManager;

namespace Dunas.Events
{
	public class TransformTriggerEventListener : GenericTriggerEventListener<Transform>
	{

		[SerializeField] private TransformEvent _response;

		private new void OnEnable() => SubscribeResponse();
		private void OnDisable() => UnsubscribeResponse();
		public override void Initialize() => UEventResponse = _response;
	}
}
