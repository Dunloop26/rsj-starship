﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Dunas.Events
{
	public class EventManager
	{
		private static EventManager _instance;
		private static EventManager Instance
		{
			get
			{
				// Si la instancia no está definida
				if (_instance == null)
				{
					// Creo una nueva instancia
					_instance = new EventManager();

					// Creo una nueva instancia
					_instance._eventDictionary = new Dictionary<string, EventHandler>();
				}

				// Retorno la instancia
				return _instance;
			}
		}

		private Dictionary<string, EventHandler> _eventDictionary;

		public static void StartListening(string eventName, EventHandler listener)
		{
			EventHandler thisEvent = null;
			if (Instance._eventDictionary.TryGetValue(eventName, out thisEvent))
			{
				thisEvent += listener;
				Instance._eventDictionary[eventName] = thisEvent;
			}
			else
			{
				thisEvent = listener;
				Instance._eventDictionary.Add(eventName, thisEvent);
			}
		}

		public static void StopListening(string eventName, EventHandler listener)
		{
			if (Instance == null) return;
			EventHandler thisEvent = null;
			if (Instance._eventDictionary.TryGetValue(eventName, out thisEvent))
			{
				thisEvent -= listener;
				Instance._eventDictionary[eventName] = thisEvent;
			}
		}

		public static void TriggerEvent(string eventName, EventArgs args, object sender = null)
		{
			EventHandler thisEvent = null;
			if (Instance._eventDictionary.TryGetValue(eventName, out thisEvent))
			{
				if(thisEvent != null) thisEvent(sender, args);
				else Debug.LogError($"The event \"{eventName}\" is null");
			}
		}

		public static void TriggerEvent<T>(string eventName, GenericEventArgs<T> args, object sender)
		{
			EventHandler thisEvent = null;
			if (Instance._eventDictionary.TryGetValue(eventName, out thisEvent))
			{
				if(thisEvent != null) thisEvent(sender, args);
				else Debug.LogError($"The event \"{eventName}\" is null");
			}
		}
	}
}
