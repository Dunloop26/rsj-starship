﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Dunas.Events
{
	public class GenericEventArgs<T> : System.EventArgs, IValue<T>
	{
		public GenericEventArgs(T value)
		{
			// Defino el valor
			Value = value;
		}

		/// <summary>
		/// Valor genérico de tipo T
		/// </summary>
		/// <value></value>
		public T Value { get; set; }
	}
}
