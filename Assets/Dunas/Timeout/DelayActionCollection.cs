﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace Dunas.Timeout
{
	public class DelayActionCollection : MonoBehaviour
	{
		[System.Serializable]
		public class DelayInfo
		{
			[Header("Settings")]
			[SerializeField]
			private string _name = string.Empty;
			[Space]
			[SerializeField]
			private UnityEvent _action = null;
			[SerializeField]
			private int _delayMilliseconds = 0;

			public UnityEvent Action { get => _action; set => _action = value; }
			public int DelayMilliseconds { get => _delayMilliseconds; set => _delayMilliseconds = value; }
			public string Name { get => _name; set => _name = value; }

			public DelayInfo WithName(string name)
			{
				Name = name;
				return this;
			}

			public DelayInfo WithAction(UnityEvent action)
			{
				Action = action;
				return this;
			}

			public DelayInfo SetDelay(int milliseconds)
			{
				DelayMilliseconds = milliseconds;
				return this;
			}
		}

		[Header("Actions")]
		[SerializeField]
		private DelayInfo[] _actions = null;

		public void FireActionName(string name)
		{
			if (string.IsNullOrEmpty(name)) return;

			var found = FindAction(name);
			if (found == null) return;

			if (found.DelayMilliseconds <= 0)
				found.Action?.Invoke();
			else
				ExecuteAction(found);
		}

		private void ExecuteAction(DelayInfo action)
		{
			Delay.SetTimeout(() => action.Action?.Invoke(), action.DelayMilliseconds);
		}

		public DelayInfo FindAction(string name)
		{
			return System.Array.Find(_actions, action => action.Name.Equals(name));
		}
	}
}