﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Dunas.Timeout
{
	public class Delay : MonoBehaviour
	{
		public class TimeoutArg
		{
			public TimeoutArg(Action callback, int milliseconds)
			{
				Callback = callback;
				Milliseconds = milliseconds;
			}

			public Action Callback { get; set; }
			public int Milliseconds { get; set; }

			// override object.Equals
			public override bool Equals(object obj)
			{
				//
				// See the full list of guidelines at
				//   http://go.microsoft.com/fwlink/?LinkID=85237
				// and also the guidance for operator== at
				//   http://go.microsoft.com/fwlink/?LinkId=85238
				//

				if (obj == null || GetType() != obj.GetType())
				{
					return false;
				}

				TimeoutArg target = (TimeoutArg)obj;
				return ActionComparer(Callback, target.Callback) && Milliseconds.Equals(target.Milliseconds);
			}

			// override object.GetHashCode
			public override int GetHashCode()
			{
				return base.GetHashCode();
			}
		}

		public List<TimeoutArg> _runningActions = new List<TimeoutArg>();
		private Delay() { }
		private static Delay _instance;
		private static Delay Instance
		{
			get
			{
				if (!_instance) _instance = CreateInstance();
				return _instance;
			}
		}

		private static Delay CreateInstance()
		{
			return new GameObject("DelayInstance").AddComponent<Delay>();
		}

		public static void SetTimeout(Action argAction, int argMilliseconds, bool forceDuplicateExecution = false)
		{
			if (argAction == null) return;

			if (argMilliseconds <= 0)
			{
				argAction?.Invoke();
			}

			if (!forceDuplicateExecution && Instance.AlreadyRunning(argAction, argMilliseconds))
			{
				Debug.LogError("Timeout Argument is already running, please wait until it's finished or force it's execution");
				return;
			}

			Instance.StartCoroutine(Instance.TimeoutCoroutine(argAction, argMilliseconds));
		}
		public static void SetTimeoutFrame(Action argAction, int frames, bool forceDuplicateExecution = false)
		{
			if (argAction == null) return;

			if (frames <= 0)
			{
				argAction?.Invoke();
			}

			if (!forceDuplicateExecution && Instance.AlreadyRunning(argAction, frames))
			{
				Debug.LogError("Timeout Argument is already running, please wait until it's finished or force it's execution");
				return;
			}

			Instance.StartCoroutine(Instance.TimeoutCoroutineFrame(argAction, frames));
		}

		private IEnumerator TimeoutCoroutine(Action argAction, int argMilliseconds)
		{
			TimeoutArg timeoutArg = new TimeoutArg(argAction, argMilliseconds);
			_runningActions.Add(timeoutArg);
			yield return new WaitForSeconds(argMilliseconds * 0.001f);
			argAction?.Invoke();

			_runningActions.Remove(timeoutArg);
		}
		private IEnumerator TimeoutCoroutineFrame(Action argAction, int frames)
		{
			TimeoutArg timeoutArg = new TimeoutArg(argAction, frames);
			_runningActions.Add(timeoutArg);

			int elapsed = 0;
			while (elapsed < frames)
			{
				elapsed++;
				yield return null;
			}

			argAction?.Invoke();
			_runningActions.Remove(timeoutArg);
		}

		private bool AlreadyRunning(Action argAction, int argMilliseconds)
		{
			int length = _runningActions.Count;
			TimeoutArg param = new TimeoutArg(argAction, argMilliseconds);

			for (int i = 0; i < length; i++)
			{
				if (param.Equals(_runningActions[i])) return true;
			}

			return false;
		}

		// Compare Delegates Action<T>
		// https://stackoverflow.com/questions/6701041/compare-delegates-actiont
		private static bool ActionComparer<T>(Action<T> firstAction, Action<T> secondAction)
		{
#if !UNITY_WEBGL

			if (firstAction.Target != secondAction.Target)
				return false;

			var firstMethodBody = firstAction.Method.GetMethodBody().GetILAsByteArray();
			var secondMethodBody = secondAction.Method.GetMethodBody().GetILAsByteArray();

			if (firstMethodBody.Length != secondMethodBody.Length)
				return false;

			for (var i = 0; i < firstMethodBody.Length; i++)
			{
				if (firstMethodBody[i] != secondMethodBody[i])
					return false;
			}
			return true;
#else
			return firstAction.Equals(secondAction);
#endif
		}


		private static bool ActionComparer(Action firstAction, Action secondAction)
		{
#if !UNITY_WEBGL
			if (firstAction.Target != secondAction.Target)
				return false;

			var firstMethodBody = firstAction.Method.GetMethodBody().GetILAsByteArray();
			var secondMethodBody = secondAction.Method.GetMethodBody().GetILAsByteArray();

			if (firstMethodBody.Length != secondMethodBody.Length)
				return false;

			for (var i = 0; i < firstMethodBody.Length; i++)
			{
				if (firstMethodBody[i] != secondMethodBody[i])
					return false;
			}
			return true;
#else
			return firstAction.Equals(secondAction);
#endif
		}

		private void OnDestroy()
		{
			_runningActions = null;
			_instance = null;
		}
	}
}