﻿using System.Collections;
using System.Collections.Generic;
using Dunas.Events;
using UnityEngine;
namespace Dunas.Extractor
{
	public class CameraDataValueExtractor : GenericDataValueExtractor<Camera>
	{
		[Header("Events")]
		[SerializeField] private CameraEvent _onDataExtracted;
		public void Initialize()
		{
			// Inicializo los elementos del extractor
			Response = _onDataExtracted;
		}
		private void OnEnable() => Initialize();
		
		public override void Extract()
		{
			// Si no ha sido inicializado
			if(!IsInitialized) Initialize();

			// Ejecuto el evento base
			base.Extract();
		}

	}
}
