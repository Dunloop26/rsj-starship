﻿using System.Collections;
using System.Collections.Generic;
using Dunas.Events;
using UnityEngine;
using UnityEngine.Events;

namespace Dunas.Extractor
{
	public abstract class GenericDataValueExtractor<T> : MonoBehaviour
	{
		[SerializeField] private ScriptableObject _scriptableData;
		[Header("Debug")]
		[SerializeField] private bool _showDebug = false;
		[Space]
		private IValue<T> _assetData;
		public IValue<T> AssetData
		{
			get
			{
				if (_assetData == null)
				{
					_assetData = _scriptableData as IValue<T>;
					if (_showDebug) Debug.Log($"Casting is {_assetData}");
				}
				return _assetData;
			}
		}
		public UnityEvent<T> Response { get; set; }
		public bool IsInitialized => Response != null && AssetData != null;

		public virtual void Extract()
		{
			// Si el camera asset data no es nulo
			if (AssetData != null)
			{
				// Obtengo el valor del asset data
				var value = AssetData.Value;

				// Si value existe
				if (value != null)
				{
					
					if (_showDebug) Debug.Log($"Extraction succeded at {this} with {value}");
					// Comunico el valor
					Response.WrapInvoke(value);
				}
				else
				{
					// Value es nulo

					Debug.LogError($"The value extracted from {AssetData} is null", this);
					return;
				}
			}
			else
			{
				// Es nulo

				Debug.LogError($"The Asset Data is not referenced", this);
				return;
			}
		}
	}
}
