﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionNotifier : MonoBehaviour
{
    public delegate void CollisionCallback(Collision other);
    public event CollisionCallback CollisionEnter;
    public event CollisionCallback CollisionStay;
    public event CollisionCallback CollisionExit;
    private void OnCollisionEnter(Collision other)
    {
        CollisionEnter?.Invoke(other);
    }

    private void OnCollisionStay(Collision other)
    {
        CollisionStay?.Invoke(other);
    }

    private void OnCollisionExit(Collision other)
    {
        CollisionExit?.Invoke(other);
    }
}
