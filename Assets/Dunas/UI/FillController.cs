﻿using System.Collections;
using Dunas.Events;
using UnityEngine;
using UnityEngine.Events;

public class FillController : MonoBehaviour
{
	[Header("Value"), Tooltip("Este campo expuesto NO ejecuta los eventos de la instancia, para ejecutarlos use Fill")]
	[SerializeField] private float _value = 0f;
	[Header("Events")]
	[SerializeField] private UnityEvent _fulfilled = null;
	[SerializeField] private UnityEvent _depleted = null;
	[SerializeField] private FloatEvent _fillChange = null;

	private float _previousValue = -1f;

	private void Start()
	{
		Fill = _value;
	}

	public float Fill
	{
		get => _value;
		set
		{
			_value = value;
			if (_value <= 0f)
				_depleted?.Invoke();
			else if (_value >= 1f)
				_fulfilled?.Invoke();

			if (_value != _previousValue)
			{
				_previousValue = _value;
				_fillChange?.Invoke(_value);
			}
		}
	}

	public void FillIn(float seconds)
	{
		StopCoroutine("FillTo");
		StartCoroutine(FillTo(1f, seconds));
	}

	public void Deplete(float seconds)
	{
		StopCoroutine("FillTo");
		StartCoroutine(FillTo(0f, seconds));
	}

	public void FillToTarget(float targetAmount, float duration)
	{
		StopCoroutine("FillTo");
		StartCoroutine(FillTo(targetAmount, duration));
	}

	private IEnumerator FillTo(float target, float seconds)
	{
		if (seconds <= 0)
		{
			Fill = target;
			yield break;
		}

		float start = Fill;
		float elapsed = 0f;
		float percent = 0f;
		while (elapsed < seconds)
		{
			elapsed += Time.deltaTime;
			percent = elapsed / seconds;
			Fill = Mathf.Lerp(start, target, percent);
			yield return null;
		}

		Fill = target;
	}
}