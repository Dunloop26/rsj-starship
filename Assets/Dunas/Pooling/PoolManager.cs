﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Dunas.Pooling
{
	public class PoolManager : MonoBehaviour
	{
		private static PoolManager _instance;
		public static PoolManager Instance
		{
			get
			{
				if (_instance == null) _instance = CreateInstance<PoolManager>("PoolManager");
				return _instance;
			}
		}

		private Dictionary<string, Pool> _poolDict = new Dictionary<string, Pool>();

		[Header("Settings")]
		[SerializeField]
		private Pool[] _premadePools;

		private void Awake()
		{
			if (_instance == null) _instance = this;
			else if (_instance != this) Destroy(gameObject);

			RegisterPool(_premadePools);
		}

		private void RegisterPool(params Pool[] pools)
		{
			int length = pools.Length;
			Pool current;
			for (int i = 0; i < length; i++)
			{
				current = pools[i];
				if (current == null) continue;
				if (string.IsNullOrEmpty(current.ID) || current.Item == null) return;

				_poolDict.Add(current.ID, current);
			}
		}

		private static T CreateInstance<T>(string name) where T : UnityEngine.MonoBehaviour
		{
			if (string.IsNullOrEmpty(name)) name = typeof(T).Name;
			return new GameObject(name).AddComponent<T>();
		}


		public static void AddPool(string id, GameObject prefab)
		{
			if (string.IsNullOrEmpty(id) || prefab == null) return;
			if (_instance._poolDict.ContainsKey(id)) return;

			_instance._poolDict.Add(id, new Pool(id, prefab));
		}

		public static Pool GetPool(string id)
		{
			if (string.IsNullOrEmpty(id)) return null;
			if (!_instance._poolDict.ContainsKey(id)) return null;

			return _instance._poolDict[id];
		}

		public static void RemovePool(string id)
		{
			if (string.IsNullOrEmpty(id)) return;
			if (!_instance._poolDict.ContainsKey(id)) return;
			_instance._poolDict.Remove(id);
		}
	}
}