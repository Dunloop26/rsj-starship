﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Dunas.Pooling
{
	[System.Serializable]
	public class Pool
	{
		public Pool(string id, GameObject item)
		{
			ID = id;
			Item = item;
		}

		[Header("Settings")]
		[SerializeField]
		private string _id = string.Empty;
		[SerializeField]
		private GameObject _item = null;

		public GameObject Item { get => _item; private set => _item = value; }
		public string ID { get => _id; private set => _id = value; }
		private Queue<GameObject> _queue = new Queue<GameObject>();
		public GameObject GetFromPool()
		{
			GameObject output = null;

			if (_queue == null)
				_queue = new Queue<GameObject>();

			int length = _queue.Count;
			for (int i = 0; i < length; i++)
			{
				output = _queue.Dequeue();
				_queue.Enqueue(output);

				if (!output.gameObject.activeInHierarchy) break;
				output = null;
			}

			if (output == null)
			{
				output = Object.Instantiate(Item, Vector3.zero, Quaternion.identity);
				output.name = $"{ID}_{length + 1}";
				_queue.Enqueue(output);
			}

			output.SetActive(true);
			return output;
		}

		public void AddToPool(GameObject item)
		{
			if (item == null) return;
			_queue.Enqueue(item);
		}
	}
}
