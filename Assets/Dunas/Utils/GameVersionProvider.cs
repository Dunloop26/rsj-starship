﻿using Dunas.Events;
using UnityEngine;

namespace Dunas.Utils
{
	public class GameVersionProvider : MonoBehaviour
	{
		[Header("Events")]
		[SerializeField] private StringEvent _atUpdate = null;
		[Header("Extra")]
		[SerializeField] private bool _updateOnAwake = true;
		[Space]
		[SerializeField] private string _versionPrefix = string.Empty;
		[SerializeField] private string _versionSufix = string.Empty;

		private void Awake()
		{
			UpdateGameVersionString();
		}

		public void UpdateGameVersionString()
		{
			_atUpdate?.Invoke(string.Format("{0}{1}{2}", _versionPrefix, Application.version, _versionSufix));
		}
	}
}