﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Dunas.Utils
{
	public class MonoListener : MonoBehaviour
	{
		[Header("Debug")]
		[SerializeField] private bool _showDebug = false;
		[Header("Events")]
		[SerializeField] private UnityEvent _onAwake = null;
		[SerializeField] private UnityEvent _onStart = null;
		[SerializeField] private UnityEvent _onEnable = null;
		[SerializeField] private UnityEvent _onDisable = null;
		[SerializeField] private UnityEvent _onDestroy = null;
		private void Awake()
		{
			_onAwake?.Invoke();
			if (_showDebug) PrintDebug(_onAwake, "Awake");
		}
		private void Start()
		{
			_onStart?.Invoke();
			if (_showDebug) PrintDebug(_onStart, "Start");
		}
		private void OnEnable()
		{
			_onEnable?.Invoke();
			if (_showDebug) PrintDebug(_onEnable, "OnEnable");
		}
		private void OnDisable()
		{
			_onDisable?.Invoke();
			if (_showDebug) PrintDebug(_onDisable, "OnDisable");
		}
		private void OnDestroy()
		{
			_onDestroy?.Invoke();
			if (_showDebug) PrintDebug(_onDestroy, "OnDestroy");
		}

		private void PrintDebug(UnityEvent unityEvent, string name)
		{
			if (HasEvents(unityEvent))
			{
				Debug.Log($"Fired {name} at {this}", this);
			}
		}

		private bool HasEvents(UnityEvent unityEvent)
		{
			return (unityEvent?.GetPersistentEventCount() ?? 0) > 0;
		}
	}
}