﻿using Dunas.Events;
using UnityEngine;

public class FloatToStringFormater : MonoBehaviour
{
	[Header("Events")]
	[SerializeField] private StringEvent _output = null;
	[Header("Settings")]
	[SerializeField] private string _format = string.Empty;

	public void Format(float input)
	{
		_output?.Invoke(input.ToString(_format));
	}
}