﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Dunas.Utils
{

  public class ErrorCatcher : MonoBehaviour
  {

    [System.Serializable]
    public class Log
    {
      public Log() { }

      [SerializeField] private string condition;
      [SerializeField] private string stackTrace;
      [SerializeField] private LogType type;

      public string Condition { get => condition; set => condition = value; }
      public string StackTrace { get => stackTrace; set => stackTrace = value; }
      public LogType Type { get => type; set => type = value; }
    }

    [System.Serializable]
    public class LogPackage
    {

      public LogPackage() { }

      [Header("Settings")]
      [SerializeField] private string _name = string.Empty;
      [Header("Logs")]
      [SerializeField] private List<Log> _logList = new List<Log>();

      public string Name { get => _name; set => _name = value; }
      public List<Log> LogList { get => _logList; set => _logList = value; }

      public void AddLog(Log log)
      {
        // Añado el log a la lista
        LogList.Add(log);
      }
    }

    public delegate void ErrorLoggerEventHandler(object sender, Log log);
    public event ErrorLoggerEventHandler OnErrorCatch;
    public event ErrorLoggerEventHandler OnAssertCatch;
    public event ErrorLoggerEventHandler OnWarningCatch;
    public event ErrorLoggerEventHandler OnLogCatch;
    public event ErrorLoggerEventHandler OnExceptionCatch;
    public event ErrorLoggerEventHandler OnAnyLogCatch;

    [Header("Settings")]
    public bool Activar = true;

    [Header("Log Packages")]
    public LogPackage errors = new LogPackage() { Name = "error" };
    public LogPackage asserts = new LogPackage() { Name = "assert" };
    public LogPackage warnings = new LogPackage() { Name = "warning" };
    public LogPackage logs = new LogPackage() { Name = "log" };
    public LogPackage exceptions = new LogPackage() { Name = "exception" };

    public void OnEnable()
    {
      DontDestroyOnLoad(gameObject);
      // Me suscribo al evento
      UnityEngine.Application.logMessageReceived += OnLogMessageReceived;
    }

    public void OnDisable()
    {
      // Me desuscribo del evento
      UnityEngine.Application.logMessageReceived -= OnLogMessageReceived;
    }

    public void OnLogMessageReceived(string condition, string stackTrace, LogType type)
    {
      // Si no está activo
      if (!Activar) return;

      // Filtro cada log y lo almaceno
      Log log = new Log() { Condition = condition, StackTrace = stackTrace, Type = type };

      if (OnAnyLogCatch != null) OnAnyLogCatch(this, log);

      switch (type)
      {
        case LogType.Error:
          if (OnErrorCatch != null) OnErrorCatch(this, log);
          errors.AddLog(log);
          break;
        case LogType.Assert:
          if (OnAssertCatch != null) OnAssertCatch(this, log);
          asserts.AddLog(log);
          break;
        case LogType.Log:
          if (OnLogCatch != null) OnLogCatch(this, log);
          logs.AddLog(log);
          break;
        case LogType.Exception:
          if (OnExceptionCatch != null) OnExceptionCatch(this, log);
          exceptions.AddLog(log);
          break;
        case LogType.Warning:
          if (OnWarningCatch != null) OnWarningCatch(this, log);
          warnings.AddLog(log);
          break;
        default:
          break;
      }
    }

  }
}