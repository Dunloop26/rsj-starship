﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Dunas.Utils
{
	public class MeshSelector : MonoBehaviour
	{
		[System.Serializable] public class MeshEvent : UnityEvent<Mesh> { }
		[Header("Values")]
		[SerializeField]
		private Mesh[] _meshValues = null;
		[Header("Events")]
		[SerializeField]
		private MeshEvent _selected = null;

		public Mesh[] MeshValues { get => _meshValues; set => _meshValues = value; }

		public void SelectRandomFromAvailable()
		{
			SelectRandom();
		}

		public void SelectRandom(int? min = null, int? max = null)
		{
			int minIndex = min ?? 0;
			int maxIndex = max ?? (MeshValues?.Length ?? 0);

			maxIndex = Mathf.Max(maxIndex, MeshValues?.Length ?? 0);

			Select(Random.Range(minIndex, maxIndex));
		}
		public void Select(int index)
		{
			if (index < 0 || index >= MeshValues?.Length) return;

			_selected?.Invoke(MeshValues[index] ?? null);
		}
	}
}
