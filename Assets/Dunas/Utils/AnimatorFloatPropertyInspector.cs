﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AnimatorFloatPropertyInspector : MonoBehaviour
{
	[System.Serializable] private class FloatEvent : UnityEvent<float> { }
	[Header("Settings")]
	[SerializeField]
	private Animator _animator = null;
	[Space]
	[SerializeField]
	private string _propertyName = string.Empty;
	[Header("Events")]
	[SerializeField]
	private UnityEvent _set = null;
	[SerializeField]
	private FloatEvent _get = null;

	public void Set(float value)
	{
		if (_animator == null) return;

		if (!string.IsNullOrEmpty(_propertyName))
		{
			_animator?.SetFloat(_propertyName, value);
			_set?.Invoke();
		}
	}

	public void Get()
	{
		_get?.Invoke(GetFloatParam());
	}

	public float GetFloatParam(string propertyName = null)
	{
		if (_animator == null) return Mathf.Infinity;
		if (string.IsNullOrEmpty(propertyName)) propertyName = _propertyName;

		return _animator?.GetFloat(_propertyName) ?? Mathf.Infinity;
	}
}