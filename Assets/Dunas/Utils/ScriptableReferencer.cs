﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptableReferencer : MonoBehaviour
{
  [Header("Settings")]
  [Tooltip("Referencia al scriptable")]
  [SerializeField] private ScriptableObject _reference;
  [Tooltip("Hacer persistente en todas las escenas")]
  [SerializeField] private bool _persistant = false;

  public static ScriptableReferencer Instance { get; set; } = null;

  public void OnEnable()
  {
    // Hacer persistente entre escenas
    if (_persistant)
    {
      // Defino la instancia
      if (Instance == null) Instance = this;
      else if (Instance != this) Destroy(gameObject);

      // Marco como persistente
      DontDestroyOnLoad(gameObject);
    }
  }
}
