﻿using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

public class ClearPlayerPrefs : MonoBehaviour
{
#if UNITY_EDITOR
	[MenuItem("Utils/Clear player prefs")]
#endif
	private static void ClearPrefs()
	{
		PlayerPrefs.DeleteAll();
	}

	public void Clear()
	{
		ClearPrefs();
	}
}
