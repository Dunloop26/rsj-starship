using System;
using Dunas.Events;
using UnityEngine;

namespace Dunas.Utils
{
	public class TransformForwardPointDistanceSampler : MonoBehaviour
	{
		[Header("Reference")]
		[SerializeField] private Transform _transformReference;

		[Header("Settings")]
		[SerializeField] private float _distanceFromCamera = 1f;

		[Header("Events")]
		[SerializeField] private Vector3Event _onPointSampled;

		public Transform TransformReference { get => _transformReference; set => _transformReference = value; }
		public float DistanceFromCamera { get => _distanceFromCamera; set => _distanceFromCamera = value; }
		public Vector3Event OnPointSampled { get => _onPointSampled; set => _onPointSampled = value; }

		public void SamplePoint()
		{
			SamplePoint(TransformReference, DistanceFromCamera, (Vector3 sampledPoint) =>
			{
				// Comunico el evento
				OnPointSampled.WrapInvoke(sampledPoint);
			});
		}

		public void SamplePoint(Transform transform, float distanceFromCamera, Action<Vector3> pointSampledCallback)
		{
			if (transform == null) return;
			if(pointSampledCallback == null) return;

			var direction = transform.forward;
			var sampledPosition = transform.position + direction * distanceFromCamera;

			pointSampledCallback(sampledPosition);
		}
	}
}