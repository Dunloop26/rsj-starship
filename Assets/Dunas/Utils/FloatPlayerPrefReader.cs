﻿using Dunas.Events;
using UnityEngine;

namespace Dunas.Utils
{
  public class FloatPlayerPrefReader : MonoBehaviour
  {
    [Header("Settings")]
    [SerializeField] private string _key = null;
    [SerializeField] private FloatEvent _reading = null;

    public void ReadCurrent()
    {
      Read(_key);
    }

    public void Read(string key)
    {
      if (string.IsNullOrEmpty(key)) return;
      if (!PlayerPrefs.HasKey(key)) return;
      _reading?.Invoke(PlayerPrefs.GetFloat(key));
    }

    public void WriteToCurrent(float value)
    {
      if (string.IsNullOrEmpty(_key)) return;
      PlayerPrefs.SetFloat(_key, value);
    }

  }
}