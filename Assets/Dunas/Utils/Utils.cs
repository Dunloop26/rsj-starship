﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Dunas.Utils
{
    public static class Utilidad
    {

        public static Transform[] ObtenerHijos(Transform target)
        {
            // Defino la salida de los datos
            List<Transform> output = new List<Transform>();

            // Recorro cada hijo de target
            foreach (Transform child in target)
            {
                // Lo añado a la lista
                output.Add(child);
            }

            // Retorno los datos
            return output.ToArray();
        }

        public static void EliminarHijos(Transform target)
        {
            foreach(Transform hijo in target){
				MonoBehaviour.Destroy(hijo.gameObject);
			}
        }
    }
}