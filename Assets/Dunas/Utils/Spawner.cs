﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Dunas.Utils
{
	public class Spawner : MonoBehaviour
	{
		[Header("References")]
		[SerializeField] private GameObject _spawnModel;
		[SerializeField] private Transform _targetParent;

		private int _spawnCount = 0;

		public GameObject SpawnModel { get => _spawnModel; set => _spawnModel = value; }
		public Transform TargetParent { get => _targetParent; set => _targetParent = value; }

		public void Spawn(Vector3 worldPosition)
		{
			_targetParent = GetTargetParentNotNull();
			Spawn(worldPosition, _spawnModel, _targetParent);
		}
		public void Spawn(Vector3 worldPosition, GameObject spawnModel, Transform targetParent)
		{
			if (_spawnModel == null) return;
			if(_targetParent == null) return;

			var instantiatedModel = Instantiate(_spawnModel, worldPosition, Quaternion.identity, _targetParent);
			_spawnCount++;

			instantiatedModel.name += _spawnCount.ToString();
		}
		private Transform GetTargetParentNotNull()
		{
			if(_targetParent == null) _targetParent = new GameObject("SpawnTarget").transform;
			return _targetParent;
		}
	}
}
