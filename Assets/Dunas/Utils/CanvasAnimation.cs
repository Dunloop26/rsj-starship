﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

namespace Dunas.Utils
{
    [System.Serializable]
    public class AnimationClipProperty
    {
        [Header("Clip Settings")]
        public string clipName = string.Empty;

        [Header("Playing Settings")]
        public bool allowPlay = true;
        public int animatorLayerIndex = 0;
        public float delay = 0.0f;
    }

    public class CanvasAnimation : MonoBehaviour
    {

        [Header("TriggerList")]
        [SerializeField] private AnimationClipProperty[] _triggers = { };

        private Queue<AnimationClipProperty> _queue = new Queue<AnimationClipProperty>();
        private CancellationTokenSource _cancellationTokenSource;

        private Animator _canvasAnimator;
        public Animator CanvasAnimator
        {
            get
            {
                if (_canvasAnimator == null) _canvasAnimator = GetComponent<Animator>();
                return _canvasAnimator;
            }
        }

        public CancellationToken CurrentToken
        {
            get
            {
                // Si el token de cancelación no es nulo
                if (_cancellationTokenSource == null)
                {
                    // Creo un nuevo token de cancelación
                    _cancellationTokenSource = new CancellationTokenSource();
                }

                // Retorno el token de cancelación
                return _cancellationTokenSource.Token;
            }
        }

        public void Cancel()
        {
            // Si el token de cancelación es nulo
            if (_cancellationTokenSource == null) return;

            // Cancelo la tarea
            _cancellationTokenSource.Cancel();

            // Renuevo el token de cancelacion
            _cancellationTokenSource.Dispose();
            _cancellationTokenSource = new CancellationTokenSource();
        }

        public void Start()
        {

            // Almaceno cada trigger en el queue
            foreach (var trigger in _triggers)
            {
                _queue.Enqueue(trigger);
            }

            // Ejecuto el queue
            ExecuteTriggerRoutine(_queue);

        }

        private void ExecuteTriggerRoutine(Queue<AnimationClipProperty> _queue)
        {
            // Si el queue existe y contiene más de un elemento
            if (_queue != null && _queue.Count > 0)
            {
                // Ejecuto una corutina encargada de validarme los tiempos de espera

                try
                {
                    // Ejecuto la llamada asíncrona
                    ExecuteTriggerRoutineAsync(_queue);
                }
                catch (Exception e) { Debug.LogException(e); }

            }
            else
            {
                if (_queue == null) Debug.LogError("El parametro queue no puede ser nulo");
                if (_queue.Count == 0) Debug.LogWarning("El queue no contiene suficientes elementos para continuar");
            }
        }

        private async Task ExecuteTriggerRoutineAsync(Queue<AnimationClipProperty> _queue)
        {
            // Verifico si ya se dió el token de cancelación
            CurrentToken.ThrowIfCancellationRequested();

            // Si el canvasAnimator es nulo
            if (CanvasAnimator == null) Cancel();

            // Mientras haya un elemento en el queue
            while (_queue.Count > 0)
            {
                // Verifico si ya se dió el token de cancelación
                CurrentToken.ThrowIfCancellationRequested();

                // Obtengo el elemento
                var currentTriggerData = _queue.Dequeue();

                // Si se permite reproducir
                if (currentTriggerData.allowPlay)
                {
                    // Ejecuto la espera
                    if (currentTriggerData.delay > float.Epsilon)
                    {
                        await Task.Delay(TimeSpan.FromSeconds(currentTriggerData.delay));
                    }

                    // Disparo el trigger 
                    CanvasAnimator.Play(currentTriggerData.clipName, currentTriggerData.animatorLayerIndex);

                }
            }

        }

    }
}