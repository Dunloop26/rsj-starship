﻿using System.Collections;
using UnityEngine;

public class CameraShake : MonoBehaviour
{
	[Header("Dependencies")]
	[SerializeField] private Camera _camera = null;

	[Header("Settings")]
	[SerializeField] private float _maxShakeDistance = 1f;
	[SerializeField] private float _duration = 1f;
	[SerializeField]
	private AnimationCurve _shakeCurve = new AnimationCurve(new Keyframe[] {
		new Keyframe(0, 1),
		new Keyframe(1, 0),
	});

	private void Awake()
	{
		if (_camera == null) _camera = GetComponent<Camera>();
	}

	public void DoShake()
	{
		StopCoroutine("CameraShakeCoroutine");
		StartCoroutine(CameraShakeCoroutine(_maxShakeDistance, _duration, _shakeCurve));
	}

	public void CustomShake(float? shakeDistance = null, float? duration = null, AnimationCurve shakeCurve = null)
	{
		shakeDistance = shakeDistance ?? _maxShakeDistance;
		duration = duration ?? _duration;
		shakeCurve = shakeCurve ?? _shakeCurve;

		StopCoroutine("CameraShakeCoroutine");
		StartCoroutine(CameraShakeCoroutine(shakeDistance.Value, duration.Value, shakeCurve));
	}

	private IEnumerator CameraShakeCoroutine(float shakeDistance, float duration, AnimationCurve shakeCurve)
	{
		if (_camera == null) yield break;
		if (_duration <= 0) yield break;

		float elapsed = 0f;
		float percent = 0f;

		Vector3 startPosition = _camera.transform.position;
		Vector3 shakeForce = Vector3.zero;

		while (elapsed < duration)
		{
			percent = elapsed / duration;
			elapsed += Time.deltaTime;

			shakeForce = Random.insideUnitSphere * shakeCurve.Evaluate(percent) * shakeDistance;
			_camera.transform.position = startPosition + shakeForce;

			yield return null;
		}

		_camera.transform.position = _camera.transform.position;
	}
}