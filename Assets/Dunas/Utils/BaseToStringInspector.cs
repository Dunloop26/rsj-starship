﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Dunas.Utils
{
  public class BaseToStringInspector : MonoBehaviour
  {
    [System.Serializable] private class StringEvent : UnityEvent<string> { }
    [Header("Events")]
    [SerializeField] private StringEvent _output = null;

    public void FloatToString(float value)
    {
      _output?.Invoke(value.ToString());
    }

    public void IntToString(int value)
    {
      _output?.Invoke(value.ToString());
    }

    public void BooleanToString(bool value)
    {
      _output?.Invoke(value.ToString());
    }

    public void ObjectToString(System.Object value)
    {
      _output?.Invoke(value.ToString());
    }

    public void UnityObjectToString(UnityEngine.Object value)
    {
      _output?.Invoke(value.ToString());
    }
  }
}