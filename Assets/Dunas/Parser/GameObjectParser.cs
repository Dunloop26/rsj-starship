﻿using System;
using System.Collections;
using System.Collections.Generic;
using Dunas.Events;
using UnityEngine;
using static Dunas.AssetEvents.Events.UnityObjectAssetEvent;

// Define el tipo de evento de la respuesta
using ResponseEventType = Dunas.Events.GameObjectEvent;

namespace Dunas.Parser
{
	public class GameObjectParser : GenericParser<GameObject>
	{
		[Header("Events")]
		[SerializeField] private ResponseEventType _onDataParsed;

		private void OnEnable() => Initialize(_onDataParsed);
	}
}
