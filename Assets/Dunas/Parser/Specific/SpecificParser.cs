﻿using System.Collections;
using System.Collections.Generic;
using Dunas.Events;
using UnityEngine;
using UnityEngine.Events;

namespace Dunas.Parser.Specific
{
	public abstract class SpecificParser<From, To> : MonoBehaviour
	{
		[Header("Value")]
		[SerializeField] private From _value;

		[Header("Events")]
		[SerializeField] private UnityEvent _onValueSet;

		public bool IsInitialized => OutUEventResponse != null;
		public From Value { get => _value; set { _value = value; OnValueSet.WrapInvoke(); } }
		public UnityEvent OnValueSet { get => _onValueSet; set => _onValueSet = value; }
		public UnityEvent<To> OutUEventResponse { get; set; }

		/// <summary>
		/// Inicializa los valores necesarios para su funcionamiento
		/// </summary>
		public abstract void Initialize();

		public virtual void Parse()
		{
			// Realizo la llamada por defecto a parse con el argumento dado
			if (Value != null)
				Parse(Value);
			else
				Debug.LogError($"The Value to parse from \"{typeof(From)}\" is null", this);
		}
		public virtual void Parse(From objectToParse)
		{
			// Obtengo la salida del método
			var output = ParseArgument(objectToParse);

			// Si no se ha inicializado el evento
			if(!IsInitialized) Initialize();

			// Si la salida no es nula
			if (output != null)
				OutUEventResponse.WrapInvoke(output);
			else Debug.LogError($"The output of type \"{typeof(To)}\" is null", this);
		}
		public abstract To ParseArgument(From ObjectToParse);
	}
}
