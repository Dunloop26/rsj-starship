﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using ResponseEvent = Dunas.Events.GameObjectEvent;

namespace Dunas.Parser.Specific
{
	public class RaycastHitToGameObjectParser : SpecificParser<RaycastHit, GameObject>
	{
		[SerializeField] private ResponseEvent _onParseResponse = new ResponseEvent();
		public override void Initialize()
		{
			// Defino las variables a inicializar
			OutUEventResponse = _onParseResponse;
		}

		public override GameObject ParseArgument(RaycastHit objectToParse)
		{
			// Genero la variable de salida
			GameObject output = null;

			// Proceso el argumento
			output = objectToParse.transform.gameObject;

			// Retorno la variable de salida
			return output;
		}
	}
}
