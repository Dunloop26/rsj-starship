﻿using System;
using System.Collections;
using System.Collections.Generic;
using Dunas.Events;
using UnityEngine;
using UnityEngine.Events;
using static Dunas.AssetEvents.Events.UnityObjectAssetEvent;

namespace Dunas.Parser
{
	public abstract class GenericParser<T> : MonoBehaviour
	{
		[Header("Settings")]
		[SerializeField] private UnityEngine.Object _objectValue;
		
		public UnityEvent<T> ParsingComunicateEvent { get; set; }
		public UnityEngine.Object ObjectValue { get => _objectValue; set => _objectValue = value; }
		public bool HasInitialized => ParsingComunicateEvent != null;

		public void Initialize(UnityEvent<T> referenceEvent)
		{
			// Defino el evento de referencia para comunicar las conversiones
			ParsingComunicateEvent = referenceEvent;
		}

		public void Parse()
		{
			// Convierto el objecto a el tipo determinado
			Parse(typeof(T).Name);
		}

		public void Parse(string customType)
		{
			// Compruebo que el objeto existe
			if (ObjectValue != null)
			{
				// Compruebo que el tipo descrito existe
				if (!string.IsNullOrEmpty(customType))
				{
					// Obtengo el tipo convertido
					var typeToConvert = Type.GetType(customType);

					// Compruebo que el tipo a convertir no sea nulo
					if (typeToConvert != null)
					{
						// Envio los datos
						T parsedObject = (T)Convert.ChangeType(ObjectValue, typeof(T));

						// Si el objeto convertido no es nulo
						if (parsedObject != null)
							// Ejecuto el evento
							ParsingComunicateEvent.WrapInvoke(parsedObject);
					}
				}
			}
		}
	}
}
