﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Dunas.DataDictionary
{
	[CreateAssetMenu(menuName = "Dunas/Data Dictionary/New SystemObject Data ID", fileName = "New SystemObject Data ID")]
	public class SystemObjectScriptableDictionaryDataID : GenericScriptableDictionaryDataID<object> { }
}
