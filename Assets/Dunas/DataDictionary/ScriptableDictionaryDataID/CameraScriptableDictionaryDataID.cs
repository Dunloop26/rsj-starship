﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Dunas.DataDictionary
{
	[CreateAssetMenu(menuName = "Dunas/Data Dictionary/New Camera Data ID", fileName = "New Camera Data ID")]
	public class CameraScriptableDictionaryDataID : GenericScriptableDictionaryDataID<Camera> { }
}
