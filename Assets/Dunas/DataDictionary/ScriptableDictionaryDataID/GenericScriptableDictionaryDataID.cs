﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Dunas.DataDictionary
{

	public abstract class GenericScriptableDictionaryDataID<T> : ScriptableObject, IValue<T>
	{
		[Header("Settings")]
		[Multiline()]
		[SerializeField] private string _devDescription;
		[SerializeField] private string _overrideIdName;
		[Header("Read-only Value")]
		[SerializeField] private T _value;
		[SerializeField] private bool _showDebug = false;
		public T Value
		{
			get
			{
				return GetValue();
			}
			set
			{
				SetValue(value);
			}
		}

		private string IDName => !string.IsNullOrEmpty(_overrideIdName) ? _overrideIdName : name;

		public void SetValue(T arg)
		{
			// Manejo el chequeo del ID nulo
			if (IDCheckNullHandle())
				return;
			// Agrego el dato
			DataDictionary.Add(IDName, arg);

			if (_showDebug) Debug.Log($"Data registered to dictionary with \"{IDName}\" and value {arg}", this);
			// Defino el valor según el argumento
			_value = arg;
		}

		public T GetValue()
		{
			// Manejo el chequeo del ID nulo
			if (IDCheckNullHandle())
				return default(T);
			// Obtengo el objeto de datos
			var dataObject = DataDictionary.Retrieve(IDName);

			// Trato de convertir el tipo del objeto obtenido a T
			try
			{
				// Intento una conversión al tipo T
				_value = (T)dataObject;
			}
			catch (System.InvalidCastException e)
			{
				// Logueo la excepción
				Debug.LogException(e);
				return default(T);
			}

			if (_showDebug) Debug.Log($"Data obtained from dictionary with \"{IDName}\" and value {_value}", this);

			// Retorno el valor almacenado
			return _value;
		}

		public void Remove()
		{
			// Manejo el chequeo del ID nulo
			if (IDCheckNullHandle())
				return;
			// Remuevo el ID actual
			DataDictionary.Remove(IDName);

			if (_showDebug) Debug.Log($"Data removed from dictionary with \"{IDName}\"", this);
			// Inicializo el valor a su estado por defecto
			_value = default(T);
		}

		private bool IDCheckNullHandle()
		{
			if (string.IsNullOrEmpty(IDName))
			{
				// Lanzo un error
				Debug.LogError($"The name ID is null or empty", this);
				return true;
			}
			else
			{
				return false;
			}
		}
	}
}
