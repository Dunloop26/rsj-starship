﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Dunas.DataDictionary
{
	[CreateAssetMenu(menuName = "Dunas/Data Dictionary/New Int Data ID", fileName = "New Int Data ID")]
	public class IntScriptableDictionaryDataID : GenericScriptableDictionaryDataID<int> { }
}
