﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Dunas.DataDictionary
{
	public class DataDictionary
	{
		private static DataDictionary _instance = null;
		public static DataDictionary Instance
		{
			get
			{
				if (_instance == null)
				{
					_instance = new DataDictionary();
					_instance._data = new Dictionary<string, object>();
				}

				return _instance;
			}
		}

		private Dictionary<string, System.Object> _data;
		public static void Add(string name, System.Object value)
		{
			if (Instance == null) return;
			if (string.IsNullOrEmpty(name))
			{
				// Lanzo un error
				Debug.LogError($"The name ID is null or empty");
				return;
			}
			
			if (Instance._data.ContainsKey(name))
			{
				Instance._data[name] = value;
			}
			else
			{
				Instance._data.Add(name, value);
			}
		}

		public static void Remove(string name)
		{
			if (Instance == null) return;
			if (string.IsNullOrEmpty(name))
			{
				// Lanzo un error
				Debug.LogError($"The name ID is null or empty");
				return;
			}
			if (Instance._data.ContainsKey(name))
			{
				Instance._data.Remove(name);
			}
		}

		public static System.Object Retrieve(string name)
		{
			if (Instance == null) return null;
			if (string.IsNullOrEmpty(name))
			{
				// Lanzo un error
				Debug.LogError($"The name ID is null or empty");
				return null;
			}
			object dataValue = null;
			if (Instance._data.TryGetValue(name, out dataValue))
			{
				if (dataValue == null) Debug.LogWarning($"The stored value \"{name}\" is null");
				return dataValue;
			}
			else
			{
				Debug.LogError($"Value not found with \"{name}\" name");
				return null;
			}
		}
	}
}
