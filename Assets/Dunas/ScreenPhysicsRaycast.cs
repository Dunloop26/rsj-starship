﻿using System.Collections;
using System.Collections.Generic;
using Dunas.Events;
using UnityEngine;

public class ScreenPhysicsRaycast : MonoBehaviour
{
	[Header("Settings")]
	[SerializeField] private Camera _raycastCamera = null;
	[SerializeField] private float _maxDistance = 24f;
	[SerializeField] private LayerMask _rayLayerMask = Physics.AllLayers;
	[SerializeField] private QueryTriggerInteraction _queryTriggerInteraction = QueryTriggerInteraction.UseGlobal;
	[Header("Events")]
	[SerializeField] private RaycastHitEvent _firstCastResultReturn = new RaycastHitEvent();
	[SerializeField] private RaycastHitArrayEvent _castResultsReturn = new RaycastHitArrayEvent();
	[Header("Debug")]
	[SerializeField] private bool _showDebug;

	/// <summary>
	/// Cámara definida como remplazo de la cámara por defecto (Camera.main)
	/// </summary>
	/// <value></value>
	public Camera RaycastCamera { get => _raycastCamera; set => _raycastCamera = value; }

	/// <summary>
	/// Obtiene la cámara objetivo del Raycast
	/// </summary>
	public Camera TargetCamera => RaycastCamera != null ? RaycastCamera : Camera.main;

	public QueryTriggerInteraction QueryTriggerInteraction { get => _queryTriggerInteraction; set => _queryTriggerInteraction = value; }
	public LayerMask RayLayerMask { get => _rayLayerMask; set => _rayLayerMask = value; }

	public void Raycast(Vector2 screenPosition)
	{
		// Obtengo la cámara
		if (TargetCamera != null)
		{
			// Genero un rayo
			var cameraRay = TargetCamera.ScreenPointToRay(screenPosition);

			// Hago un raycast con el rayo
			var results = Physics.RaycastAll(cameraRay, _maxDistance, RayLayerMask, QueryTriggerInteraction);

			string logMsg = $"Raycast with {results.Length} results";

			// Si hay al menos un resultado
			if (results.Length > 0)
			{
				logMsg += $", with first result as {results[0].transform}";
				// Comunico los eventos
				_firstCastResultReturn.WrapInvoke(results[0]);
				_castResultsReturn.WrapInvoke(results);
			}

			if (_showDebug) Debug.Log($"{logMsg}", this);
		}
		else
		{
			// La cámara objetivo es nula
			Debug.LogError($"Target camera is null", this);
		}
	}
}
